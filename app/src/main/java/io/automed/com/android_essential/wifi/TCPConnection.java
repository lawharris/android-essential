package io.automed.com.android_essential.wifi;

import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * this class handles the low level TCP connection.
 * it manages the socket (open,close)
 * its main function is to send and receive byte arrays.
 */

public class TCPConnection extends Thread {
    public interface OnReadListener {
        void onRead(byte[] response, int len);
    }

    Socket socket;
    String address;
    int port;
    OnReadListener listener = null;
    DataOutputStream out;
    InputStream in;
    boolean Running;

    public TCPConnection(String address, int port) {
        this.address = address;
        this.port = port;
        this.Running = false;
    }

    /**
     * send a packet over the current connection
     *
     * @param packet the packet to send
     *
     * @see TcpPacket
     */
    public void SendPacket(TcpPacket packet){
        try{
            int connTimeOut=0;
            while(!Running) {
                try {
                    connTimeOut++;
                    Thread.sleep(100);
                    if(connTimeOut==5)
                        break;
                }catch(InterruptedException e)
                {
                    Log.e("Send Packet","Connection Time Out" + e);
                }
            }
            //send packet
            // moved to new thread to prevent crashes in Android 7 (which prevents network on main thread)
            final TcpPacket finalPacket = packet;
            Thread thread = new Thread(new Runnable(){
                public void run() {
                    try {
                        if(out==null){
                            Log.e("SendPacket", " Out = null");
                        }else {
                            out.write(finalPacket.GetBuffer(), 0, finalPacket.GetBuffer().length);
                        }
                        return;
                    } catch (IOException e) {
                        Log.e("SendPacket", " Error", e);
                    }
                }
            });

            thread.start();
        }catch (Exception ex){

        }
    }

    public void forceClosingSocket(){
        try {
            if(socket!=null){
                Log.d("TCP Task", " Closed");
                Running=false;
                out.flush();
                out.close();
                in.close();
                socket.close();
            }
        } catch (Exception e){
            Log.e("TCP Task", " Error", e);
        }
    }

    public boolean checkDeviceConnected(){return Running;}

    @Override
    public void run() {
        try {
            socket = new Socket(InetAddress.getByName(address), port);
            socket.setSoTimeout(3000);
            out = new DataOutputStream(socket.getOutputStream());
            in = socket.getInputStream();

            byte[] Buffer = new byte[128];
            int len=0;
            while (socket.isConnected() && !Thread.currentThread().isInterrupted()) {
                //read response with timeout
                //Log.d("SetupCheckBound",String.valueOf(socket.isBound()));
                if(socket.isBound()){
                    Running = true;
                }
                try {
                    DataInputStream dis = new DataInputStream(in);
                    len=dis.read(Buffer);
                    if(len== -1) {
                        //Log.d("TCP Task", " Closed");
                        Running=false;
                        out.flush();
                        out.close();
                        in.close();
                        socket.close();
                        break;
                    }else{

                        if(len>0){
                            if (listener != null) {
                                listener.onRead(Buffer,len);
                            }
                        }
                    }
                }catch(SocketTimeoutException | SocketException e){
                   //Log.d("TCP Task", " Time Out");
                }catch (Exception e){
                    Log.e("TCP Task", " Error", e);
                }
//                finally {
//                }
            }
            if(socket!=null){
                Log.d("TCP Task", "Stopping");
                Running=false;
                out.flush();
                out.close();
                in.close();
                socket.close();
            }
        } catch (Exception e) {
            Log.e("TCPConnection","error in TCP",e);
        }
    }

    //Sets a listener for data reception events
    public void setListener(OnReadListener listener) {
        this.listener = listener;
    }
}