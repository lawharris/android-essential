package io.automed.com.android_essential.model.object.api;

public class LdMedInventory_Base {
    public String id;
    public double quantity;
    public double volume;
    public double last_local_volume;
    public double local_quantity_used;
    public String createdAt;
    public String updatedAt;
    public String batchNo;
    public int amId;
    public String amProduct;
    public double amDose;
    public short amMinDose;
    public short amMaxDose;
    public char amMethod;
    public String expiry;
    public String prescription_number;
    public Boolean active;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getVolume() { return volume; }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getLocalVolume() { return last_local_volume; }

    public void setLocalVolume(double last_local_volume) {
        this.last_local_volume = last_local_volume;
    }

    public void setLocalQuantityUsed(double local_quantity_used) {
        this.local_quantity_used = local_quantity_used;
    }

    public double getLocalQuantityUsed() { return local_quantity_used; }


    public void setCreatedAt(String createdAt){
        this.createdAt = createdAt;
    }
    public String getCreatedAt(){
        return this.createdAt;
    }
    public void setUpdatedAt(String updatedAt){
        this.updatedAt = updatedAt;
    }
    public String getUpdatedAt(){
        return this.updatedAt;
    }
    public void setBatchNo(String batchNo){
        this.batchNo = batchNo;
    }
    public String getBatchNo(){
        return this.batchNo;
    }
    public void setAmId(int amId){
        this.amId = amId;
    }
    public int getAmId(){
        return this.amId;
    }
    public void setAmProduct(String amProduct){
        this.amProduct = amProduct;
    }
    public String getAmProduct(){
        return this.amProduct;
    }
    public void setAmDose(double amDose){
        this.amDose = amDose;
    }
    public double getAmDose(){
        return this.amDose;
    }
    public void setAmMindose(short amMinDose){
        this.amMinDose = amMinDose;
    }
    public short getAmMindose(){
        return this.amMinDose;
    }
    public void setAmMaxdose(short amMaxDose){
        this.amMaxDose = amMaxDose;
    }
    public short getAmMaxdose(){
        return this.amMaxDose;
    }
    public void setAmMethod(String amMethod){
        this.amMethod = (char)(amMethod.getBytes()[0]);
    }
    public char getAmMethod(){
        return Character.toUpperCase(this.amMethod);
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public Boolean getActive(){ return active;}

    public void setActive(Boolean active) {this.active = active;}

    public String getPrescriptionNumber() {return prescription_number;}

    public void setPrescription_number(String prescription_number){this.prescription_number = prescription_number;}

    public LdMedInventory_Base(){

    }

    public LdMedInventory_Base(LdMedInventory_Base SrcObj){
        this.id = SrcObj.getId();
        this.quantity = SrcObj.getQuantity();
        this.volume = SrcObj.getVolume();
        this.last_local_volume = SrcObj.getLocalVolume();
        this.local_quantity_used = SrcObj.getLocalQuantityUsed();
        this.createdAt = SrcObj.getCreatedAt();
        this.updatedAt = SrcObj.getUpdatedAt();
        this.batchNo = SrcObj.getBatchNo();
        this.amId = SrcObj.getAmId();
        this.amProduct = SrcObj.getAmProduct();
        this.amDose = SrcObj.getAmDose();
        this.amMinDose = SrcObj.getAmMindose();
        this.amMaxDose = SrcObj.getAmMaxdose();
        this.amMethod = SrcObj.getAmMethod();
        this.expiry = SrcObj.getExpiry();
        this.active = SrcObj.getActive();
    }
}
