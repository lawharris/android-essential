package io.automed.com.android_essential.model.object.automed;

import io.automed.com.android_essential.utils.AppMethods;
import io.automed.com.android_essential.wifi.TcpPacket;

public class AdapterState {
    private byte[] data;

    public AdapterState(TcpPacket packet){
        data = packet.getData();
    }

    public byte getError(){
        return data[1];
    }

    public byte getState(){
        return data[0];
    }

    public int getAmId(){
        byte[] am_id = new byte[4];
        System.arraycopy(data, 2, am_id, 0, data.length-2);
        return AppMethods.bytesToInt(am_id);
    }
}
