package io.automed.com.android_essential.model.object.api;

public class LdMedInventory extends LdMedInventory_Base{

    private String user;
    private String farm;

    public void setUser(String user){
        this.user = user;
    }
    public String getUser(){
        return this.user;
    }
    public void setFarm(String farm){
        this.farm = farm;
    }
    public String getFarm(){
        return this.farm;
    }

    public LdMedInventory(){

    }

    public LdMedInventory(LdMedInventory SrcObj){
        this.id = SrcObj.getId();
        this.quantity = SrcObj.getQuantity();
        this.volume = SrcObj.getVolume();
        this.last_local_volume = SrcObj.getLocalVolume();
        this.local_quantity_used = SrcObj.getLocalQuantityUsed();
        this.createdAt = SrcObj.getCreatedAt();
        this.updatedAt = SrcObj.getUpdatedAt();
        this.batchNo = SrcObj.getBatchNo();
        this.user = SrcObj.getUser();
        this.farm = SrcObj.getFarm();
        this.amId = SrcObj.getAmId();
        this.amProduct = SrcObj.getAmProduct();
        this.amDose = SrcObj.getAmDose();
        this.amMinDose = SrcObj.getAmMindose();
        this.amMaxDose = SrcObj.getAmMaxdose();
        this.amMethod = SrcObj.getAmMethod();
        this.expiry = SrcObj.getExpiry();
        this.active = SrcObj.getActive();
    }
}
