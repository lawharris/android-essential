package io.automed.com.android_essential.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import io.automed.com.android_essential.R;
import io.automed.com.android_essential.model.object.api.LdTreatment;
import io.automed.com.android_essential.utils.AppMethods;

import java.util.ArrayList;
import java.util.List;

public class RecordsAdapter extends BaseAdapter {
    private final LayoutInflater mInflator;
    private List<LdTreatment> mItems;
    private Context mContext;

    private class ViewHolder {
        TextView tvDate, tvEvent, tvID, tvWeight, tvVID;
    }

    public RecordsAdapter(Context context) {
        mContext = context;
        mInflator = LayoutInflater.from(mContext);
        mItems = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public LdTreatment getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup viewParent) {
        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.activity_record_item, viewParent, false);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        if (viewHolder == null) {
            viewHolder = new ViewHolder();

            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.tvEvent = (TextView) convertView.findViewById(R.id.tv_dose);
            viewHolder.tvWeight = (TextView) convertView.findViewById(R.id.tv_weight);
            viewHolder.tvID = (TextView) convertView.findViewById(R.id.tv_id);
            viewHolder.tvVID = (TextView) convertView.findViewById(R.id.tv_vid);

            convertView.setTag(viewHolder);
        }

        LdTreatment item = getItem(pos);

        viewHolder.tvVID.setText(item.getVid());

        viewHolder.tvDate.setText(AppMethods.toLocalDateFromServerGMTDate(item.getTrDate()));

        viewHolder.tvWeight.setText(String.valueOf(item.getWeight())+" "+item.getUnit());
        viewHolder.tvEvent.setText(item.getHumanTrDoseFormattedForCountry(mContext));
        viewHolder.tvID.setText(item.getAnimalID());

        if(item.getIncomplete()==2|| item.getIncomplete()==3){
            viewHolder.tvVID.setTextColor(mContext.getResources().getColor(R.color.retreated_dose));
            viewHolder.tvWeight.setTextColor(mContext.getResources().getColor(R.color.retreated_dose));
            viewHolder.tvEvent.setTextColor(mContext.getResources().getColor(R.color.retreated_dose));
            viewHolder.tvID.setTextColor(mContext.getResources().getColor(R.color.retreated_dose));
            viewHolder.tvDate.setTextColor(mContext.getResources().getColor(R.color.retreated_dose));
        }
        else if(item.getIncomplete()==1){
            viewHolder.tvVID.setTextColor(mContext.getResources().getColor(R.color.incomplete_dose));
            viewHolder.tvWeight.setTextColor(mContext.getResources().getColor(R.color.incomplete_dose));
            viewHolder.tvEvent.setTextColor(mContext.getResources().getColor(R.color.incomplete_dose));
            viewHolder.tvID.setTextColor(mContext.getResources().getColor(R.color.incomplete_dose));
            viewHolder.tvDate.setTextColor(mContext.getResources().getColor(R.color.incomplete_dose));
        }
        else{
            viewHolder.tvVID.setTextColor(Color.BLACK);
            viewHolder.tvWeight.setTextColor(Color.BLACK);
            viewHolder.tvEvent.setTextColor(Color.BLACK);
            viewHolder.tvID.setTextColor(Color.BLACK);
            viewHolder.tvDate.setTextColor(Color.BLACK);
        }

        /*viewHolder.tvDate.setTypeface(((RecordsActivity)mContext).amNormalType);
        viewHolder.tvDrug.setTypeface(((RecordsActivity)mContext).amNormalType);
        viewHolder.tvEvent.setTypeface(((RecordsActivity)mContext).amNormalType);
        viewHolder.tvWeight.setTypeface(((RecordsActivity)mContext).amNormalType);
        viewHolder.tvID.setTypeface(((RecordsActivity)mContext).amBoldType);*/
        return convertView;
    }

    public void addItem(LdTreatment item) {
        mItems.add(0, item);
        notifyDataSetChanged();
    }

    public void setItems(List<LdTreatment> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public List<LdTreatment> getItems(){
        return mItems;
    }

    public void clearItems() {
        mItems.clear();
    }
}
