package io.automed.com.android_essential.database;

public class LdTreatmentCP {
    public static final String TABLE_LD_TREATMENTS = "ld_treatments";
    public static final String KEY_ID = "key_id";
    public static final String KEY_TR_DOSE = "key_tr_dose";
    public static final String KEY_TR_RELDATE = "key_tr_reldate";
    public static final String KEY_TR_DATE = "key_tr_date";
    public static final String KEY_TR_SERIALNO = "key_tr_serialno";
    public static final String KEY_TR_FIRMWARE = "key_tr_firmware";
    public static final String KEY_TR_AMODNO = "key_tr_amodno";
    public static final String KEY_TR_BATCHNO = "key_tr_batchno";
    public static final String KEY_AM_ID = "key_am_id";
    public static final String KEY_LD_LIFEDATA = "key_ld_lifedata";
    public static final String KEY_USER = "key_user";
    public static final String KEY_FARM = "key_farm";
    public static final String KEY_AMB_TEMP = "key_amb_temp";
    public static final String KEY_SERIALNO_ADAP = "key_ser_adap";
    public static final String KEY_WEIGHT = "key_weight";
    public static final String KEY_VID = "key_vid";
    public static final String KEY_REASON = "reason";
    public static final String KEY_INCOMPLETE = "incomplete";
    public static final String KEY_UNIT = "unit";

    public static final String CREATE_TABLE_LD_TREATMENTS =
            "CREATE TABLE "+ TABLE_LD_TREATMENTS + "("
                    + KEY_ID + " TEXT PRIMARY KEY, "
                    + KEY_TR_DOSE + " TEXT, "
                    + KEY_TR_RELDATE + " TEXT, "
                    + KEY_TR_DATE + " TEXT, "
                    + KEY_TR_SERIALNO + " TEXT, "
                    + KEY_TR_FIRMWARE + " TEXT, "
                    + KEY_TR_AMODNO + " TEXT, "
                    + KEY_TR_BATCHNO + " TEXT, "
                    + KEY_AM_ID+ " INTEGER, "
                    + KEY_LD_LIFEDATA + " TEXT, "
                    + KEY_USER + " INTEGER, "
                    + KEY_FARM + " TEXT, "
                    + KEY_AMB_TEMP+ " INTEGER, "
                    + KEY_SERIALNO_ADAP+ " TEXT, "
                    + KEY_WEIGHT+ " REAL, "
                    + KEY_VID+ " TEXT, "
                    + KEY_INCOMPLETE+" INTEGER, "
                    + KEY_REASON+ " TEXT, "
                    + KEY_UNIT+ " TEXT"
                    + ")";


}
