package io.automed.com.android_essential.wifi;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;


/**
 * this class detects the device that connects to the hotspot
 * the file "/proc/net/arp" is parsed to get the ip and mac addresses for these devices
 * the scan is run in an AsyncTask, the result is returned in a list of ClientScanResult
 *
 * @see ClientScanResult
 */

public class ConnectionManagerTask extends AsyncTask<ArrayList<ClientScanResult>, Void, ArrayList<ClientScanResult>> {
    ArrayList<ClientScanResult> ResultOut;

    public interface ScanFinishListener {
        void onFinished(ArrayList<ClientScanResult> result);
    }

    private final ScanFinishListener finishListener;

    public ConnectionManagerTask(ScanFinishListener listener) {
        // The listener reference is passed in through the constructor
        this.finishListener = listener;
    }

    protected ArrayList<ClientScanResult> doInBackground(ArrayList<ClientScanResult>... params) {
        BufferedReader br = null;
        final ArrayList<ClientScanResult> result = new ArrayList<ClientScanResult>();
        ResultOut = params[0];
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if ((splitted != null) && (splitted.length >= 4)) {
                    // Basic sanity check
                    String mac = splitted[3];

                    if (mac.matches("..:..:..:..:..:..")) {
                        boolean isReachable = InetAddress.getByName(splitted[0]).isReachable(1000);
                        //boolean isReachable = true;
                        String log;
                        log = new String(" found :"+ splitted[0] + " " + splitted[3] + " " + splitted[5]);
                        if (isReachable) {
                            log+=" Connected";
                            result.add(new ClientScanResult(splitted[0], splitted[3], splitted[5]));
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.toString());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), e.getMessage());
            }
        }
        return result;
    }


    // invoked on the UI thread before the task is executed. This step is normally used
    // to setup the task, for instance by showing a progress bar in the user interface.
    protected void onPreExecute(){
    }

    protected void onPostExecute(ArrayList<ClientScanResult> result) {
        super.onPostExecute(result);

        ResultOut.clear();
        for(int i=0; i<result.size();i++)
            ResultOut.add(result.get(i));

        if(this.finishListener != null) {
            this.finishListener.onFinished(result);
        }
    }

}
