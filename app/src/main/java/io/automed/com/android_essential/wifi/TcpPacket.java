package io.automed.com.android_essential.wifi;

/**
 * creates a byte array representing a tcp packet.
 * this class can be used in one of two ways, either create a packet from
 * an existing buffer(when a packet is received for example), in this case the class can be used
 * to parse the different fields in the packet.
 * The second case is to use the class to create a buffer to be transmitted to the device.
 * in this case the different fields are provided to the constructor.
 * the information about the possible packets can be found at this page
 * https://automed-os.atlassian.net/wiki/display/AMA/Device+to+Apps+-+Communication+Spec
 */
public class TcpPacket {
    private byte[] Data;
    private String IP;
    private int Delay; //how long to wait for an answer

    public TcpPacket(byte[] Buffer, int Len)
    {
        Data = new byte[Len];
        System.arraycopy(Buffer, 0, Data, 0, Len);
    }

    public int getDelay() {
        return Delay;
    }

    public String getIP() {
        return IP;
    }

    public TcpPacket(String IP, byte ClassID, byte CommandID, int Length, byte[] data, int delay)
    {
        int datLen = 0;
        this.Delay = delay;
        this.IP = IP;

        if(data != null)
            datLen = data.length;

        Data = new byte[4+datLen];

        Data[0]= ClassID;
        Data[1]= CommandID;
        Data[3]= (byte)(Length >> 8);
        Data[2]= (byte)Length;

        if(data != null)
            System.arraycopy(data, 0, Data, 4, data.length);
    }

    public int getId(){
        int id=0;
        id = (int)Data[0]<<8 | Data[1];
        return id;
    }

    public byte GetClassID()
    {
        return Data[0];
    }

    public byte GetCommandID()
    {
        return Data[1];
    }

    public int getDataLength(){
        int i =0;
        i= Data[2]<<8 | Data[3];
        return i;
    }

    public byte[] GetBuffer(){
        return Data;
    }

    public byte[] getData(){
        int len = getDataLength();
        if(Data.length !=len+4)
            return null;

        byte[] buf = new byte[len];
        System.arraycopy(Data, 4, buf, 0, len);
        return buf;
    }
}
