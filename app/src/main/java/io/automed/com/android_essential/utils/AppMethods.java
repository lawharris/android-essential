package io.automed.com.android_essential.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import io.automed.com.android_essential.R;
import io.automed.com.android_essential.views.SplashActivity;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class AppMethods {


    public static String secondsToDateTime(long Seconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Seconds*1000);
        return formatter.format(calendar.getTime());
    }

    /**
     * converts a date string to unix epoch in secons
     * @param dateStr string date "yyyy-MM-dd HH:mm:ss"
     * @return number of second since 1970
     */
    public static int DateTimeToSeconds(String dateStr)
    {
        int retSecs = 0;
        Date date = null;

        Log.d("DateTimeToSeconds", "parse string: " + dateStr);
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault()).parse(dateStr);
            Log.d("DateTimeToSeconds", "format yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
//            e.printStackTrace();
        }
        if (date == null) {
            try {
                date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                        Locale.getDefault()).parse(dateStr);
                Log.d("DateTimeToSeconds", "yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            } catch (ParseException e) {
//                e.printStackTrace();
            }
        }
        if (date == null) {
            try {
                date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                        Locale.getDefault()).parse(dateStr);
                Log.d("DateTimeToSeconds", "format yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            } catch (ParseException e) {
//                e.printStackTrace();
            }
        }

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
            retSecs = (int) (calendar.getTimeInMillis() / 1000);
        }
        return retSecs;
    }

    /**
     * get datetime
     * */
    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDateServerFormat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        return dateFormat.format(date);
    }


    public static String fromServerDate(String dateTime){
        Date date = new Date();
        if(dateTime==null)
        {
            return new String("0000-00-00 00:00:00");
        }
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = dateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat outputFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //outputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        outputFormat.setTimeZone(TimeZone.getDefault());
        return outputFormat.format(date);
    }

    public static String fromServerDate(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);

    }

    public static String toLocalDateFromServerGMTDate(String simpleDateString){
        Date date = null;
        if(simpleDateString==null)
        {
            return new String("0000-00-00 00:00:00");
        }
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = dateFormat.parse(simpleDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format(date);
    }

    public static String GMTEpochToServerDate(Long epoch) {

        Date epochDate = new Date(epoch);
        return toServerDate(epochDate);

    }


    public static String toServerDate(Date date){
        if(date==null)
        {
            return new String("0000-00-00'T'00:00:00.000'Z'");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }

    public static String toServerDateFromSimpleDateString(String dateTime){
        Date date = null;
        if(dateTime==null)
        {
            return new String("0000-00-00'T'00:00:00.000'Z'");
        }
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault()).parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if( date != null ) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            //dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(date);
        }
        return new String("0000-00-00'T'00:00:00.000'Z'");
    }

    public static String ExpiryDateFormat(String dateTime){
        Date date = null;
        if(dateTime==null)
        {
            return new String("1970-01-01"); // if the function was called with no string
        } else if (dateTime.isEmpty()) {
            return new String("1970-01-01"); // if the string was empty
        } else if (dateTime.contentEquals("0000-00-00'T'00:00:00.000'Z'")) {
            return new String("1970-01-01"); // if the string was the 'unparsable' 0 value string
        }
        else {
            try {

                SimpleDateFormat serverInputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
                //serverInputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                serverInputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                date = serverInputFormat.parse(dateTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());

            return dateFormat.format(date);
        }
    }

    public static Date ServerStringToDate(String dateTime){
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            date = dateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void hideKeyboard(Context context, View view){
        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static String md5(String input) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(input.getBytes(), 0, input.length());
        BigInteger i = new BigInteger(1, m.digest());

        return String.format("%1$032x", i);
    }

    public static String bytesToHex(byte[] in, int len) {
        final StringBuilder builder = new StringBuilder();
        for(int i=0;i<len;i++) {
            builder.append(String.format("%02x", in[i]));
        }
        return builder.toString();
    }

    public static int bytesToInt(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    public static short bytesToShort(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getShort();
    }

    public static String bytesToString(byte[] bytes){
        return new String(bytes);
    }

    public static byte[] intToBytes(int value){
        ByteBuffer buffer = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
        buffer.clear();
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(value);
        return buffer.array();
    }

    public static byte[] shortToBytes(short value){
        ByteBuffer buffer = ByteBuffer.allocate(Short.SIZE / Byte.SIZE);
        buffer.clear();
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.putShort(value);
        return buffer.array();
    }

    public static String loadJSONFromAsset(Context context, String file) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    public static void showDialog(Context context, String title, String message){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        if (title!=null)
            builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.alert_ok, null);
        builder.show();
    }

//    public static void prepareButton(Context context, Button button, int drawable){
//        Drawable dbScan = context.getResources().getDrawable(drawable);
//        dbScan.setBounds(0, 0, (int) (dbScan.getIntrinsicWidth() * 0.5),
//                (int) (dbScan.getIntrinsicHeight() * 0.5));
//        ScaleDrawable sdScan = new ScaleDrawable(dbScan, 0, 60, 60);
//        button.setCompoundDrawables(null, sdScan.getDrawable(), null, null);
//    }

    public static void createShortcut(Context context){
        Intent shortcutIntent = new Intent(context, SplashActivity.class);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(context, R.mipmap.ic_launcher));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        context.sendBroadcast(addIntent);
    }
}
