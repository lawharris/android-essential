package io.automed.com.android_essential.model.object.aida;

public class LdTreatment_AIDA {
    private String treatmentDate;
    private String userID;
    private long animalID;
    private String drugName;
    private String drugBatch;
    private double dose;
    private double animalWeight;


    public void setTreatmentDate(String treatmentDate){
        this.treatmentDate = treatmentDate;
    }
    public String getTreatmentDate(){
        return this.treatmentDate;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }
    public String getUserID(){
        return this.userID;
    }

    public void setAnimalID(long animalID){
        this.animalID = animalID;
    }
    public long getAnimalID(){
        return this.animalID;
    }

    public void setDrugName(String drugName){
        this.drugName = drugName;
    }
    public String getDrugName(){
        return this.drugName;
    }

    public void setDrugBatch(String drugBatch){
        this.drugBatch = drugBatch;
    }
    public String getDrugBatch(){
        return this.drugBatch;
    }

    public void setDose(double dose){
        this.dose = dose;
    }
    public double getDose(){
        return this.dose;
    }

    public double getAnimalWeight() {
        return animalWeight;
    }
    public void setAnimalWeight(double animalWeight) {
        this.animalWeight = animalWeight;
    }


}
