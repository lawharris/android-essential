package io.automed.com.android_essential.model.environments.bluetooth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import io.automed.com.android_essential.bluetooth.BluetoothConnection;
import io.automed.com.android_essential.model.object.api.LdMedInventory;
import io.automed.com.android_essential.views.MenuActivity;
import io.automed.com.android_essential.wifi.ConnectedDevices;
import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by harris on 30/01/18.
 */
public class Ranger2100_Induction extends Thread {
    public interface OnReadListener {
        void onRead(byte[] response, int len);
    }
    private BluetoothConnection Connection;
    private Context mAppContext;
    private ArrayList<ConnectedDevices> ConnDevices;
    private ArrayList<ConnectedDevices> SelectedDevices;
    private Boolean waitingForScaleToZeroBeforeNextAnimal = false;
    private Boolean overrideWeightWait = true;
    private String unit_string = "";
    private String unit_object = "";
    private OnReadListener listener = null;
    private MenuActivity mAct;
    private String BluetoothJsonString = "";

    public Ranger2100_Induction(MenuActivity mAct, BluetoothConnection Connection, Context AppContext, ArrayList<ConnectedDevices> ConnDevices ){
        this.Connection = Connection;
        mAppContext = AppContext;
        this.ConnDevices = ConnDevices;
        SelectedDevices = new ArrayList<>();
        this.mAct = mAct;
    }

    public void setMilPerKg(final ConnectedDevices dev)
    {
        int max = dev.getAdapSettings().getMaxDose();

        RelativeLayout linearLayout = new RelativeLayout(mAppContext);
        final int maxAdapterDose = max;
        final EditText doseEditText = new EditText(mAppContext);
        doseEditText.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        doseEditText.addTextChangedListener( new TextWatcher() {

            public void beforeTextChanged(CharSequence a,int b,int c,int d) {
            }

            public void onTextChanged(CharSequence a,int b,int c,int d) {
            }

            public void afterTextChanged(Editable s) {

            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams doseValueParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        doseValueParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(doseEditText,doseValueParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mAppContext);
        alertDialogBuilder.setTitle("Select 1 mL per kg value for D"+dev.getId());
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                try {
                                    dev.setWeightDose(Double.parseDouble(doseEditText.getText().toString()));
                                } catch ( NumberFormatException nfe ) {
                                    Log.e("RecordsActivity","Unable to convert doseEditText from string to double");
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setConnectedDevices(){
        if(SelectedDevices.size()>0){
            SelectedDevices.clear();
        }
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(mAppContext);
        DevList.setTitle("Select Applicators");
        String[] devicesName = new String[ConnDevices.size()];
        final boolean[] selectedDevices = new boolean[ConnDevices.size()];

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                mAppContext,
                android.R.layout.simple_list_item_multiple_choice);
        for(int i = 0; i < devicesName.length; i++) {
            devicesName[i] = "D"+ String.valueOf(ConnDevices.get(i).getId());
            selectedDevices[i] = false;
            setMilPerKg(ConnDevices.get(i));
        }
        DevList.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        DevList.setMultiChoiceItems(
                devicesName,
                selectedDevices,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        SelectedDevices.add(ConnDevices.get(indexSelected));
                    }
                }).setPositiveButton("Ok",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int ii) {

            }
        }).setCancelable(false).create();

        DevList.show();
    }
    public void setConnectedDevices(ConnectedDevices dev){
        boolean contains = SelectedDevices.contains(dev);
        if(contains == false){
            SelectedDevices.add(dev);
            setMilPerKg(dev);
        }
    }
    @Override
    public void run() {
        Connection.setListener(new BluetoothConnection.OnReadListener() {
            @Override
            public void onRead(final byte[] response, final int len) {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }

                Log.d("onBluetoothReadLn", String.valueOf(len));
                String Str = new String(Arrays.copyOfRange(response, 0, len), StandardCharsets.UTF_8);
                BluetoothJsonString += Str;
                if(BluetoothJsonString.indexOf("G")!=-1 && BluetoothJsonString.indexOf("     ")!=1)
                {
                    for(ConnectedDevices dev:SelectedDevices) {
                        if (overrideWeightWait) {
                            dev.setVarWeightNextDose(true);
                            waitingForScaleToZeroBeforeNextAnimal = false;
                        }

                        if (dev.getVarWeightNextDose()) {
                            int indexTerminator = BluetoothJsonString.indexOf("G");

                            Log.d("Index of G", String.valueOf(indexTerminator));

                            String rawWeight = BluetoothJsonString.substring(indexTerminator - 6, indexTerminator);

                            Log.d("Raw Weight", rawWeight);

                            BluetoothJsonString = "";
                            String numberOnly = rawWeight.replaceAll("[^0-9\\.]+", "");
                            double weight = 0.0;
                            try {
                                weight = Double.parseDouble(numberOnly);
                            } catch (NumberFormatException nfe) {
                                Log.e("RecordsActivity", "Unable to convert weight from string to double");
                            }
                            Log.d("Stable weight", String.valueOf(weight));
                            if ((weight > 15 && !waitingForScaleToZeroBeforeNextAnimal) || overrideWeightWait) {  // common sense testing so a 0kg value doesn't trigger a dose calibration
                                // don't send another weight until this animal clears the scales and they are reset to 0kg
                                waitingForScaleToZeroBeforeNextAnimal = true;

                                dev.setWeight(weight);
                                //((TextView) findViewById(R.id.quickDisplay_weight_value)).setText(String.valueOf(weight) + " kg");
                                dev.SetVarWeightMode();

                                final LdMedInventory med = dev.getDefaultMed();
                                //Return the number of Kg per Mil
                                double wdose = dev.getWeightDose();
                                //                                db.closeDB();
                                Double fullDoseToAdminister = 0.0;
                                SharedPreferences shared_unit = mAppContext.getSharedPreferences("unit", mAppContext.MODE_PRIVATE);
                                unit_string = shared_unit.getString("unit_object", null);
                                unit_object = new Gson().fromJson(unit_string, String.class);
                                if (unit_object != null && unit_object.equalsIgnoreCase("lbs")) {
                                    fullDoseToAdminister = new Double((weight * 0.453592) / wdose);
                                    dev.setUnit("lbs");
                                } else {
                                    fullDoseToAdminister = new Double(weight / wdose);
                                    dev.setUnit("kg");
                                }
                                fullDoseToAdminister = round(fullDoseToAdminister, 2);
                                Double remainingDoseToAdminister = new Double(fullDoseToAdminister);
                                //remainingDoseToAdminister = 3.5;
                                med.setAmDose(fullDoseToAdminister);// update the dose
                                dev.setTotalDose(fullDoseToAdminister);
                                mAct.setDoseDisplay(String.valueOf(fullDoseToAdminister),dev.getId());

                                Double maximumAdapterDose = new Double(remainingDoseToAdminister);
                                if (dev.getAdapSettings() != null) {
                                    // testing bluetooth scales comparing dose to max dose potential null issue

                                    maximumAdapterDose = 1.00 * dev.getAdapSettings().getMaxDose();

                                    // if we have a valid weight-based dose we now configure the device to administer it
                                    // if the required dose is less than the maximum our adapter can administer then we can just configure the device and administer it
                                    // if the requried dose exceeds the maximum for our adapter we need to allow for multiple administrations

                                    // currently only works up to 2x maximum adapter dose

                                    if (remainingDoseToAdminister > maximumAdapterDose) {
                                        // issue a full adapter as the main dose
                                        med.setAmDose(maximumAdapterDose);
                                        remainingDoseToAdminister -= maximumAdapterDose;
                                        LdMedInventory remMed = new LdMedInventory(med);
                                        remMed.setAmDose(remainingDoseToAdminister);
                                        dev.setRemainingMed(remMed);
                                    } else {
                                        // issue actual dose as only dose, no remaining dose
                                        med.setAmDose(remainingDoseToAdminister);
                                        dev.setRemainingMed(null);

                                    }
                                    //Turn rfid on again if it is individual mode
                                    if(dev.getIndividualRFIDMode() == 1 && dev.isRfidOn() == false){
                                        dev.TurnRfidOn();
                                        try{
                                            Thread.sleep(500);
                                        } catch(Exception e){

                                        }
                                    }
                                    dev.ConfigureAdapter(med, true, mAppContext);
                                }
                                overrideWeightWait = false;
                            } else if (weight < 15) {
                                waitingForScaleToZeroBeforeNextAnimal = false;
                                Log.d("Discard low weight", String.valueOf(weight));
                            } else {
                                Log.d("Ignore weight", String.valueOf(weight));
                            }
                        } else {
                            // we have a stable reading but we're not ready for a new dose, so just ignore
                            Log.d("StblWght Not Ready Dose", Str);
                        }
                    }
                }
                else// unstable reading
                {
                    for(ConnectedDevices dev:SelectedDevices) {
                        if (dev.isVarWeightOn()) {
                            if (dev.getVarWeightDoseDone()) {
                                dev.setVarWeightNextDose(true);
                                dev.setVarWeightDoseDone(false);
                            }
                        }
                    }
                }
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                }
            }
        });
    }

    public void stopEnvironment(){
        interrupt();
        Connection.disconnectBluetooth();
        SelectedDevices.clear();
    }
    private void showToast(final String message, final Context context) {
        Handler h = new Handler(mAppContext.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void resetWeight(){
        waitingForScaleToZeroBeforeNextAnimal = false;
        overrideWeightWait = true;
        mAct.setWeightDisplay("0.0");
        mAct.setDoseDisplay("0");
    }

    public void setListener(OnReadListener listener) {
        this.listener = listener;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
