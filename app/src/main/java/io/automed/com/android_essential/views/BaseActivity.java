package io.automed.com.android_essential.views;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.automed.com.android_essential.R;
import io.automed.com.android_essential.database.DbHelper;
import io.automed.com.android_essential.model.object.api.LdMedInventory;
import io.automed.com.android_essential.model.object.api.LdTreatment;
import io.automed.com.android_essential.utils.AppConstants;
import io.automed.com.android_essential.wifi.AutomedAPIServer;
import io.automed.com.android_essential.wifi.ClientScanResult;
import io.automed.com.android_essential.wifi.ConnectedDevices;
import io.automed.com.android_essential.wifi.ConnectionManagerTask;

public abstract class BaseActivity extends AppCompatActivity {
    protected ArrayList<ConnectedDevices> ConnDevices = new ArrayList<ConnectedDevices>();
    private Handler handler = new Handler();
    private boolean isHandlerRunning = false;
    private String network_string = "";
    private String network_object = "";
    static Boolean AlertListOn = false;
    private WifiConfiguration netConfig = null;
    private ArrayList<ClientScanResult> OldScan = new ArrayList<ClientScanResult>();
    private ArrayList<ClientScanResult> NewScan = new ArrayList<ClientScanResult>();
    private ConnectionManagerTask cmt;
    private Context mContext;
    private String liquid_unit;
    private boolean isStarted = false;
    private AutomedAPIServer automedAPIServer;
    private static final int DEFAULT_PORT = 8080;
    private String doseString = "";
    private AlertDialog alertNoticeDialog;

    public enum WIFI_AP_STATE {
        WIFI_AP_STATE_DISABLING,
        WIFI_AP_STATE_DISABLED,
        WIFI_AP_STATE_ENABLING,
        WIFI_AP_STATE_ENABLED,
        WIFI_AP_STATE_FAILED
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("AIDA","Server resuming...");
        //performSyncUponLogin(this);
        if (!isStarted) {
            startAutomedAPIServer();
        }
    }

    private boolean startAutomedAPIServer() {
        // permissions
        Log.d("AIDA","Server starting...");
        isStarted = true;
        int port = getPortFromEditText();
        try {
            if (port == 0) {
                throw new Exception();
            }
            automedAPIServer = new AutomedAPIServer(port);
            automedAPIServer.mContext = this;
            automedAPIServer.start();
            Log.d("AIDA IP Address", getIpAccess());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("API ERROR", e.toString());
            //Snackbar.make(coordinatorLayout, "The PORT " + port + " doesn't work, please change it between 1000 and 9999.", Snackbar.LENGTH_LONG).show();
        }
        return false;
    }

    private int getPortFromEditText() {
        String valueEditText = "8080";
        return (valueEditText.length() > 0) ? Integer.parseInt(valueEditText) : DEFAULT_PORT;
    }

    private String getIpAccess() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        Log.d("API Server", formatedIpAddress);
        return "http://" + formatedIpAddress + ":";
    }

    protected abstract int getLayoutResource();
    public void setLiquidUnit(String liquid_unit){
        this.liquid_unit = liquid_unit;
    }
    public void setContext(Context mContext){
        this.mContext = mContext;
    }
    public Boolean getIsHandlerRunning(){
        return isHandlerRunning;
    }
    public void setIsHandlerRunning(Boolean isHandlerRunning){this.isHandlerRunning = isHandlerRunning;}
    public void turnOffScan(){
        isStarted = false;
        handler.removeCallbacks(PeriodicNetScan);
        ConnDevices.clear();
        NewScan.clear();
        OldScan.clear();
    }

    public void turnOffScanWithoutFullCleaning(){
        handler.removeCallbacks(PeriodicNetScan);
    }

    public void showToast(final String message, final Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showAlert(String message, String title, Context context) {
        if(!AlertListOn){
            alertNoticeDialog = new AlertDialog.Builder(context).create();
            alertNoticeDialog.setTitle(title);
            alertNoticeDialog.setMessage(message);
            alertNoticeDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            AlertListOn = false;
                        }
                    });
            alertNoticeDialog.show();
            AlertListOn = true;
        }

    }

    public void closeAlert(){
        if(alertNoticeDialog!=null && alertNoticeDialog.isShowing()){
            AlertListOn = false;
            alertNoticeDialog.dismiss();
        }
    }

    public void voidPreviousTreatmentAndReconfigureDose(Context context) {

        DbHelper db = DbHelper.getInstance(context);

        ConnectedDevices dev = null;
        for (int i = 0; i < ConnDevices.size(); i++) {

            dev = ConnDevices.get(i);
            List<LdTreatment> ldLdTreatmentList = db.getLatestTreatment();
            LdTreatment previousTreatment = ldLdTreatmentList.get(0);
            final LdMedInventory med = dev.getDefaultMed();

            if(med!=null && previousTreatment.getTrSerialno().equalsIgnoreCase(dev.getMAC())) {
                //TODO: code changed
                if(dev.getDosingMode() == true && dev.getActionMode() == true){
                    showToast("Device D"+dev.getId()+" is currently in action.",context);
                }else if(dev.getDosingMode() == false && dev.getActionMode() == true){

                    db.updateLdTreatmentIncompleteStatus(previousTreatment.getId(),2);
                    dev.setanimalEID(previousTreatment.getAnimalID());
                    dev.setanimalVID(previousTreatment.getVid());
                    dev.setRetreatedFlag(true);


                    if (previousTreatment.getWeight() > 0){
                        dev.setWeight(previousTreatment.getWeight());
                        //TODO: Trying passing in as parameter
                        // if (rfpa != null) {
                        //     rfpa.setWeightDisplay(String.valueOf(previousTreatment.getWeight()));
                        // }
                        Double remainingDoseToAdminister = previousTreatment.getTrDose();
                        Double maximumAdapterDose = 1.00 * dev.getAdapSettings().getMaxDose();
                        dev.setTotalDose(remainingDoseToAdminister);
                        setDoseDisplay(String.valueOf(previousTreatment.getTrDose()),dev.getId());
                        setWeightDisplay(String.valueOf(previousTreatment.getWeight()));
                        // if (rfpa != null) {
                        //     rfpa.setDoseDisplay(String.valueOf(remainingDoseToAdminister));
                        // }
                        if (remainingDoseToAdminister > maximumAdapterDose) {
                            med.setAmDose(maximumAdapterDose);
                            remainingDoseToAdminister -= maximumAdapterDose;
                            LdMedInventory remMed = new LdMedInventory(med);
                            remMed.setAmDose(remainingDoseToAdminister);
                            dev.setRemainingMed(remMed);
                        } else {
                            med.setAmDose(remainingDoseToAdminister);
                            dev.setRemainingMed(null);
                        }
                        if(dev.getDoseType().equals("weight")){
                            dev.SetVarWeightMode();
                        }
                    } else {
                        Double previousDoseToReAdminister = previousTreatment.getTrDose();
                        med.setAmDose(previousDoseToReAdminister);// update the dose
                        if (dev.getAdapSettings() != null) {
                            dev.setRemainingMed(null);
                        }
                    }
                    dev.ConfigureAdapter(med, true, context);
                }
            }
        }
    }

    private WIFI_AP_STATE CheckAccessPointState(){
        try {
            WifiManager mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            Method method = mWifiManager.getClass().getMethod("getWifiApState");

            int tmp = ((Integer)method.invoke(mWifiManager));

            // Fix for Android 4
            if (tmp >= 10) {
                tmp = tmp - 10;
            }
            WIFI_AP_STATE state = WIFI_AP_STATE.class.getEnumConstants()[tmp];
            return state;
        } catch (Exception e) {
            Log.e(this.getClass().toString(), "", e);
            return WIFI_AP_STATE.WIFI_AP_STATE_FAILED;
        }
    }

    Runnable PeriodicNetScan = new Runnable() {
        @Override
        public void run() {
            if(CheckAccessPointState()!=WIFI_AP_STATE.WIFI_AP_STATE_ENABLED){
                Log.d("PeriodWifiScan", "not enabled");
                handler.removeCallbacks(PeriodicNetScan);// disable scan
                AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Enable WiFi Access Point?\n(This will disconnect your usual WiFi network)");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                createWifiAccessPoint();
                                handler.postDelayed(PeriodicNetScan, AppConstants.PERIOD_NET_SCAN);// enable scan
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            } else {
                Log.d("PeriodWifiScan", "enabled");
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                scanConnectedDevices();
                handler.postDelayed(this, AppConstants.PERIOD_NET_SCAN);
            }
        }
    };

    private void createWifiAccessPoint() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissions = new ArrayList<String>();


            int hasWiFiPermission = checkSelfPermission(Manifest.permission.CHANGE_WIFI_STATE);

            if (hasWiFiPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CHANGE_WIFI_STATE);

            }
            int hasChangeNetworkState = checkSelfPermission(Manifest.permission.CHANGE_NETWORK_STATE);

            if (hasChangeNetworkState != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CHANGE_NETWORK_STATE);

            }
            int hasWriteSettings = checkSelfPermission(Manifest.permission.WRITE_SETTINGS);

            permissions.add(Manifest.permission.WRITE_SETTINGS);



            if (!permissions.isEmpty()) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_SETTINGS}, 111);
            }

            int nowHasWriteSettings = checkSelfPermission(Manifest.permission.WRITE_SETTINGS);



            if (!Settings.System.canWrite(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + this.getPackageName()));
                startActivity(intent);
            }
        }

        Method[] wmMethods = wifiManager.getClass().getDeclaredMethods();
        boolean methodFound = false;
        for (Method method : wmMethods) {
            if (method.getName().equals("setWifiApEnabled")) {
                methodFound = true;
                SharedPreferences shared_network = getSharedPreferences("network", MODE_PRIVATE);
                network_string = shared_network.getString("network_object",null);
                network_object = new Gson().fromJson(network_string, String.class);
                netConfig = new WifiConfiguration();
                //netConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                if(network_object==null){
                    netConfig.SSID = "AutoMed";  // default key
                    //netConfig.SSID = "automed1";  // brenneman test 1
                    //netConfig.SSID = "automed2";  // brenneman test 2
                }else{
                    netConfig.SSID = network_object;  // default key
                }
                netConfig.preSharedKey = "12345678";
                netConfig.networkId = 07120204;
                try {
                    boolean apstatus = (Boolean) method.invoke(wifiManager, netConfig, true);
                    for (Method isWifiApEnabledmethod : wmMethods) {
                        if (isWifiApEnabledmethod.getName().equals("isWifiApEnabled")) {
                            while (!(Boolean) isWifiApEnabledmethod.invoke(wifiManager)) {
                            }
                            for (Method method1 : wmMethods) {
                                if (method1.getName().equals("getWifiApState")) {
                                    int apstate;
                                    apstate = (Integer) method1.invoke(wifiManager);
                                }
                            }
                        }
                    }
                } catch (IllegalArgumentException e) {
                    Log.e("HotSpot",e.toString());
                } catch (IllegalAccessException e) {
                    Log.e("HotSpot", e.toString());
                } catch (InvocationTargetException e) {
                    Log.e("HotSpot", e.toString());
                }
            }
        }
        if (!methodFound) {
            AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Your phone's API does not contain setWifiApEnabled method to configure an access point");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    private void scanConnectedDevices() {
        cmt = new ConnectionManagerTask(new ConnectionManagerTask.ScanFinishListener() {
            @Override
            public void onFinished(ArrayList<ClientScanResult> result) {
                // no device detected
                if(result.size()==0){
                    for(Iterator<ClientScanResult> iterator = OldScan.iterator(); iterator.hasNext();) {
                        ClientScanResult tmp1 = iterator.next();
                        showToast("Device Disconnected\n Mac Address: "+ tmp1.getMAC()+ "\nIP Address: "+tmp1.getIP(),mContext);
                        //remove the element from the list
                        if( ConnDevices.size() > OldScan.indexOf(tmp1) ) {
                            ConnDevices.get(OldScan.indexOf(tmp1)).Stop();
                        }
                        iterator.remove();
                    }
                    ConnDevices.clear();
                    return;
                }

                // delete the dead devices
                // element in Oldscan that are not found in newscan result
                for(Iterator<ClientScanResult> iterator = OldScan.iterator(); iterator.hasNext();) {
                    boolean found = false;
                    ClientScanResult tmp1 = iterator.next();
                    for (ClientScanResult tmp2 : result) {
                        if (tmp1.equals(tmp2)) {
                            found = true;
                            result.remove(tmp2);
                            // test the next element
                            break;
                        }
                    }
                    // element doesn't exist in the current scan
                    if (found == false) {
                        showToast("Device Disconnected\n Mac Address: "+ tmp1.getMAC()+ "\nIP Address: "+tmp1.getIP(),mContext);
                        for (ConnectedDevices tmp3 : ConnDevices) {
                            if (tmp3.getDeviceAddr().equals(tmp1)) {
                                ConnDevices.remove(tmp3);
                                break;
                            }
                        }
                        //remove the dead element from the list
                        iterator.remove();
                    }
                }


                // add the new scanned devices
                for(ClientScanResult tmp1:result){
                    // new device detected
                    OldScan.add(tmp1);
                    ConnDevices.add(new ConnectedDevices(tmp1,13000,getApplicationContext()));
                    showToast("New Device Connected\n Mac Address: "+ tmp1.getMAC()+ "\nIP Address: "+tmp1.getIP(),mContext);
                }
            }
        });

        cmt.execute(NewScan);
    }


    public void reEnableWIFI(Context mContext){
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        if(CheckAccessPointState()==WIFI_AP_STATE.WIFI_AP_STATE_ENABLED)
        {
            Method method = null;
            try {
                method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
                method.invoke(wifiManager, netConfig, false);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            // now re-enable standard WiFi operation
            if (!wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(true);
            }
        }
    }

    public void setEIDDisplay(final String EID) {

        Log.d("QUICK DISPLAY", "EID Level 1");

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_EID_value)).setText(EID);
                Log.d("QUICK DISPLAY", "EID "+ EID);
            }
        });

        Log.d("QUICK DISPLAY", "EID Level 1a");
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_EID_value)).setText(EID);
                Log.d("QUICK DISPLAY", "UI Thread EID "+ EID);

            }
        });
    }

    public void setVIDDisplay(final String vid) {

        Log.d("QUICK DISPLAY", "EID Level 1");

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_vid_value)).setText(vid);
                Log.d("QUICK DISPLAY", "vid "+ vid);
            }
        });

        Log.d("QUICK DISPLAY", "EID Level 1a");
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_vid_value)).setText(vid);
                Log.d("QUICK DISPLAY", "UI Thread vid "+vid);

            }
        });
    }

    public void setDoseDisplay(final String dose,int device_id) {
        doseString+=("D"+String.valueOf(device_id)+": "+dose+liquid_unit+",\n ");
        Log.d("QUICK DISPLAY", "EID Level 1");
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_dose_value)).setText(doseString);
                Log.d("QUICK DISPLAY", "dose "+ dose);
            }
        });

        Log.d("QUICK DISPLAY", "EID Level 1a");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_dose_value)).setText(doseString);
                Log.d("QUICK DISPLAY", "UI Thread dose "+ dose);

            }
        });
    }

    public void setDoseDisplay(final String dose) {
        if(Double.parseDouble(dose)<=0){
            doseString = "";
        } else{
            doseString=dose+liquid_unit;
        }

        Log.d("QUICK DISPLAY", "EID Level 1");
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_dose_value)).setText(doseString);
                Log.d("QUICK DISPLAY", "dose "+ dose);
            }
        });

        Log.d("QUICK DISPLAY", "EID Level 1a");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_dose_value)).setText(doseString);
                Log.d("QUICK DISPLAY", "UI Thread dose "+ dose);

            }
        });
    }

    public void clearDisplays(){
        setVIDDisplay("000");
        setEIDDisplay("000000000000000");
        setWeightDisplay("0");
        setDoseDisplay("0");

    }


    public void setWeightDisplay(final String weight) {
        Log.d("QUICK DISPLAY", "Weight Level 1");

        new Handler().post(new Runnable() {

            @Override
            public void run() {
                ((TextView) findViewById(R.id.quickDisplay_weight_value)).setText(weight);
                Log.d("QUICK DISPLAY", "Weight "+ weight);
            }
        });

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ((TextView) findViewById(R.id.quickDisplay_weight_value)).setText(weight);
                Log.d("QUICK DISPLAY", "UI Thread Weight "+ weight);

            }
        });
    }


    public ArrayList<ConnectedDevices> getConnDevices() {
        return ConnDevices;
    }

    public void switchHotspotNetwork (MenuItem menuItem){

    }

    public void resetScan (MenuItem menuItem){

    }

    public void aboutClicked (MenuItem menuItem){

    }
}
