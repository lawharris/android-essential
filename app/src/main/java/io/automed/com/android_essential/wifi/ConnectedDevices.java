package io.automed.com.android_essential.wifi;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import io.automed.com.android_essential.R;

import io.automed.com.android_essential.bluetooth.BluetoothConnection;
import io.automed.com.android_essential.database.DbHelper;
import io.automed.com.android_essential.model.object.api.LdMedInventory;
import io.automed.com.android_essential.model.object.api.LdTreatment;
import io.automed.com.android_essential.model.object.automed.AdapterSettings;
import io.automed.com.android_essential.model.environments.json.StockaID;
import io.automed.com.android_essential.utils.AppConstants;
import io.automed.com.android_essential.utils.AppConstants.MedTypes;
import io.automed.com.android_essential.utils.AppMethods;
import com.google.gson.Gson;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by David on 18-02-16.
 * class to represent the connected device
 * each newly detected device will have one class created
 * this class will hold all the information about the device
 * and has an interface to interract with the device
 */
public class ConnectedDevices implements Parcelable {
    private enum DevStates{
        STATE_SET_ID,
        STATE_SET_TIME,
        STATE_GET_SYS_INFO,
        STATE_GET_MEM_INFO,
        STATE_GET_ADAP_SETT,
        STATE_SET_FARM_INFO,
        STATE_DONE,
    }

    public static int getReadDelay() {
        return ReadDelay;
    }

    private static int ReadDelay =5000;
    protected static int count = 0;
    protected static ArrayList<DeviceID> ConnDevIDs=new ArrayList<DeviceID>();

    private ClientScanResult deviceAddr = null;
    private int fwVer;
    private int id;
    private String MAC;
    private DevStates state;
    private boolean isDataReady; //indicate if all data is read
    private boolean rfidOn;
    private boolean primeOn = false;
    private int TotalMemSize;
    private int UsedMemSize;

    private TCPConnection Connection;
    private int port;
    private String farmId;
    private Context mAppContext;
    private LdMedInventory MedInventory;
    private AdapterSettings AdapSettings;
    private Boolean VarWeightOn;
    private Boolean VarWeightNextDose;
    private Boolean VarWeightDoseDone;
    private Boolean NewAdapter;
    private Boolean WaitingForNewAdapterSetup;
    private double calibrationDosingSteps;
    private double weight;
    private String reason = "";
    private String unit = "";
    private String animalEID;
    private String animalVID;
    private LdMedInventory remainingMed;
    private LdMedInventory defaultMed;
    private Boolean deviceIsInGroupRFIDMode = false;
    private Boolean deviceIsGroupRFIDMaster = false;
    private double totalDose = 0.0;
    private boolean dosingMode = false;
    private boolean actionMode = false;
    private boolean noMedMode = false;
    private boolean newMedFlag = false;
    private String doseType = "";
    private int rfidMode = 0;
    private int incompleteFlag = 0;
    private boolean retreatedFlag = false;
    private double weightDose = 1000000000;

    private boolean returnStringToBTScale = false;
    private String BTdeviceName = "";
    private BluetoothConnection bc = null;

    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeightDose(){return weightDose;}
    public void setWeightDose(double weightDose){this.weightDose = weightDose;}

    public void setReturnStringToBTScale(boolean returnStringToBTScale, BluetoothConnection bc, String BTdeviceName){
        this.returnStringToBTScale = returnStringToBTScale;
        this.bc = bc;
        this.BTdeviceName = BTdeviceName;
    }

    public TCPConnection getConnection() {
        return Connection;
    }

    public double getTotalDose() { return totalDose; }
    public void setTotalDose(double totalDose) { this.totalDose = totalDose; }

    public Boolean getRetreatedFlag(){
        return retreatedFlag;
    }

    public void setRetreatedFlag(Boolean retreatedFlag){
        this.retreatedFlag = retreatedFlag;
    }

    public String getDoseType(){return doseType;}
    public void setDoseType(String doseType){this.doseType = doseType;}

    public int getIndividualRFIDMode(){ return rfidMode; }
    public void setIndividualRFIDMode(int rfidMode){
        this.rfidMode = rfidMode;
    }

    public boolean getDosingMode(){ return dosingMode; }
    public void setDosingMode(boolean dosingMode){ this.dosingMode = dosingMode; }

    public boolean getActionMode(){ return actionMode; }

    public void setReason(String reason){
        this.reason = reason;
    }

    public void setUnit(String unit){
        this.unit = unit;
    }

    public String getanimalEID() {
        return animalEID;
    }
    public void setanimalEID(String animalEID) {
        this.animalEID = animalEID;
    }

    public String getanimalVID() {
        return animalVID;
    }
    public void setanimalVID(String animalVID) {
        this.animalVID = animalVID;
    }

    public LdMedInventory getRemainingMed() {
        return remainingMed;
    }
    public void setRemainingMed(LdMedInventory remainingMed) {
        this.remainingMed = remainingMed;
    }

    public LdMedInventory getDefaultMed() {
        return defaultMed;
    }
    //TODO: predefined a med when connected
    public void setDefaultMed(LdMedInventory defaultMed) {
        this.defaultMed = defaultMed;
    }

    public int getTotalMemSize() {
        return TotalMemSize;
    }

    public int getUsedMemSize() {
        return UsedMemSize;
    }

    public boolean isRfidOn() {
        return rfidOn;
    }
    public boolean isPrimeOn(){
        return primeOn;
    }

    public ClientScanResult getDeviceAddr() {
        return deviceAddr;
    }

    public int getFwVer() {
        return fwVer;
    }

    public String getFwVerString() {
        int low = fwVer&0x00FF;
        int high = (fwVer&0xFF00)>>8;
        return String.format("V%d.%02d",high,low);
    }

    public int getId() {
        return id;
    }

    public String getMAC() {return MAC;}

    public DevStates getState() {
        return state;
    }



    public void AskDeviceForAdapterSettingIfNeeded(Context context) {
        if (AdapSettings == null) {
            // first time send adapter settings
            Log.d("Sending Packet", "040D Adapter Settings");
            TcpPacket AdapConfChkPck = new TcpPacket(deviceAddr.getIP(), (byte) 0x04, (byte) 0x0D, 0, null, ReadDelay);
            Connection.SendPacket(AdapConfChkPck);
        }
    }

    /**
     * send the adapter configuration message to the device
     *
     * @param selLdMedInventory : the currently selected medication
     */
    //Temp fix for non-working factory settings command
    public void preConfigureAdapter(LdMedInventory selLdMedInventory, Context context){
        ByteBuffer setup_b = ByteBuffer.allocate(32);
        setup_b.order(ByteOrder.LITTLE_ENDIAN);
        setup_b.putInt(selLdMedInventory.getAmId());
        setup_b.putShort((short)1);
        setup_b.putShort(selLdMedInventory.getAmMindose());
        setup_b.putShort(selLdMedInventory.getAmMaxdose());
        setup_b.put((byte) selLdMedInventory.getAmMethod());
        setup_b.put((byte) 0);
        setup_b.putInt(AppMethods.DateTimeToSeconds(selLdMedInventory.getCreatedAt()));
        setup_b.put(selLdMedInventory.getBatchNo().getBytes());
        TcpPacket AdapConfPck = new TcpPacket(deviceAddr.getIP(), (byte) 0x04, (byte) 0x0B, setup_b.capacity(), setup_b.array(), ReadDelay);
        Connection.SendPacket(AdapConfPck);
        NewAdapter = false;
        WaitingForNewAdapterSetup = true;

        ConfigureAdapter(getDefaultMed(),false,mAppContext);
    }

    public void resetAdapter(){
        ByteBuffer setup_b = ByteBuffer.allocate(32);
        String model = "VNGL999";
        String batchNo = "z9999999";
        setup_b.order(ByteOrder.LITTLE_ENDIAN);
        setup_b.put(model.getBytes());
        setup_b.put((byte) 'V');
        setup_b.put(batchNo.getBytes());
        setup_b.putInt(0);
        setup_b.putInt(0);
        setup_b.putInt(0);
        TcpPacket AdapConfPck = new TcpPacket(deviceAddr.getIP(), (byte) 0x04, (byte) 0x01, setup_b.capacity(), setup_b.array(), ReadDelay);
        Connection.SendPacket(AdapConfPck);
    }

    public void ConfigureAdapter(LdMedInventory selLdMedInventory, Boolean justSend, Context context)
    {
        DbHelper db = DbHelper.getInstance(mAppContext);
        mAppContext = context;
        newMedFlag = false;
        noMedMode = false;

        if( deviceAddr == null ) {
            DisplayToast("device not connected");
            return;
        }

        if(justSend)// send the selected medication as is
        {
            // send the number of steps to the device
            // check if adapter already programmed
            // if not then send dose = steps (ie ensure what we send as steps is less than the maxDose)
            // if so then we need to calculate steps based upon the max dose in the adapter
            int totalSleep = 0;
            int sleepDelay = 500;

            if (AdapSettings == null) {
                // first time send adapter settings
                Log.d("Sending Packet", "040D Adapter Settings");
                TcpPacket AdapConfChkPck = new TcpPacket(deviceAddr.getIP(), (byte) 0x04, (byte) 0x0D, 0, null, ReadDelay);
                Connection.SendPacket(AdapConfChkPck);

            }
            while ( AdapSettings == null ) {
                Log.d("Wait For AdapSettings", String.valueOf(totalSleep));

                if (totalSleep > 2000) {
                    // if we've been waiting for more than 5 seconds and the device isn't programmed yet, just abort
                    break;
                } else {
                    try {
                        // wait for a period of time to get response from device
                        totalSleep = totalSleep + sleepDelay;
                        Thread.sleep(sleepDelay);
                    } catch (Exception e) {
                        break;
                    }
                }
            }


            int steps = 0;

            ByteBuffer b = ByteBuffer.allocate(32);
            b.order(ByteOrder.LITTLE_ENDIAN);
            b.putInt(selLdMedInventory.getAmId());

            //calculate steps by querying database
            steps = db.getStepsForDoseAndAdapterType(selLdMedInventory.getAmDose(), getAdapSettings().getAdapterTypeString(), getAdapSettings().getMaxDose());
            b.putShort((short)steps);
            b.putShort(selLdMedInventory.getAmMindose());
            b.putShort(selLdMedInventory.getAmMaxdose());
            b.put((byte) selLdMedInventory.getAmMethod());
            b.put((byte) 0);
            int creationDateStamp = AppMethods.DateTimeToSeconds(selLdMedInventory.getCreatedAt());
            b.putInt(creationDateStamp);
            b.put(selLdMedInventory.getBatchNo().getBytes());
            TcpPacket AdapConfPck = new TcpPacket(deviceAddr.getIP(), (byte) 0x04, (byte) 0x0B, b.capacity(), b.array(), ReadDelay);
            Connection.SendPacket(AdapConfPck);
            MedInventory = selLdMedInventory;
        }
        else
        {
            Intent i = new Intent("android.intent.action.AUTOMED.type").putExtra(AppConstants.HANDLER_MSG_TYPE, AppConstants.MSGTypes.MSG_TYPE_WEIGHT_SELECT.ordinal());
            i.putExtra(AppConstants.HANDLER_AGE_MSG_MEDID,selLdMedInventory.getId());
            i.putExtra(AppConstants.HANDLER_AGE_MSG_DEVID, this.getId());
            mAppContext.sendBroadcast(i);
        }
    }

    public void SetManualDoseMode(){
        // send command to reset the device to manual mode
        //TcpPacket Pck = new TcpPacket(deviceAddr.getIP(),(byte)0x04, (byte)0x14, 0, null,50);
        //Connection.SendPacket(Pck);
        VarWeightOn = false;
        VarWeightNextDose = false;
        VarWeightDoseDone = false;
    }

    public void SetVarWeightMode(){
        TcpPacket Pck = new TcpPacket(deviceAddr.getIP(),(byte)0x04, (byte)0x14, 0, null,50);
        Connection.SendPacket(Pck);
        VarWeightOn = true;
        VarWeightNextDose = false;
        VarWeightDoseDone = false;
    }

    public void SetBootloaderMode(){
        TcpPacket BootPck = new TcpPacket(deviceAddr.getIP(),(byte)0x02, (byte)0x07, 0, null,50);
        Connection.SendPacket(BootPck);
    }

    public void Prime(){
        if(this.rfidOn == true){
            DisplayToast("Prime Not Available During RFID Scanning");
        }else{
            primeOn = !primeOn;
            TcpPacket PrimePck = new TcpPacket(deviceAddr.getIP(),(byte)0x04, (byte)0x0E, 0, null,50);
            Connection.SendPacket(PrimePck);
        }
    }

    public void InvalidateMedConf(){
        TcpPacket Pck = new TcpPacket(deviceAddr.getIP(),(byte)0x04, (byte)0x11, 0, null,50);
        Connection.SendPacket(Pck);
    }

    public void DisplayConfMedication(){
        TcpPacket PrimePck = new TcpPacket(deviceAddr.getIP(),(byte)0x04, (byte)0x13, 0, null,50);
        Connection.SendPacket(PrimePck);
    }

    public void Beep(){
        //beep
        TcpPacket BeepPck = new TcpPacket(deviceAddr.getIP(),(byte)0x02, (byte)0x0C, 0, null,50);
        Connection.SendPacket(BeepPck);
    }

    //outline: turn RFID Reader on
    public void TurnRfidOn() {
        if(primeOn == false){
            this.rfidOn = true;
            TcpPacket RFIDOnPck = new TcpPacket(deviceAddr.getIP(),(byte)0x03, (byte)0x09, 0, null,50);
            Connection.SendPacket(RFIDOnPck);
        }else{
            DisplayToast("RFID Scanning Not Available During Prime");
        }
    }

    //outline: turn RFID Reader off
    public void TurnRfidOff() {
        this.rfidOn = false;
        TcpPacket RFIDOffPck = new TcpPacket(deviceAddr.getIP(),(byte)0x03, (byte)0x0A, 0, null,50);
        Connection.SendPacket(RFIDOffPck);
    }

    public ConnectedDevices(ClientScanResult scan, int Port, Context context){
        boolean found =false;
        mAppContext = context;
        for(DeviceID devid:ConnDevIDs){
            if (devid.getMAC().equals(scan.getMAC())){
                found = true;
                id = devid.getId();
            }
        }
        MAC = scan.getMAC();
        if(found == false){
            count++; //use this  as device id
            id = count;
            ConnDevIDs.add(new DeviceID(scan.getMAC(),id));
        }
        VarWeightOn = false;
        VarWeightNextDose = true;
        VarWeightDoseDone = false;
        this.farmId = farmId;
        isDataReady = false;
        rfidOn = false;
        state = DevStates.STATE_SET_ID;
        this.deviceAddr = scan;
        this.port = Port;
        Connection = new TCPConnection(this.deviceAddr.getIP(),this.port);
        Connection.setListener(new TCPConnection.OnReadListener() {
            @Override
            public void onRead(final byte[] response, final int len) {
                Log.d("onRead", bytesToHex(response,len));
                processPacket(response, len);
            }
        });
        Connection.start();
        //Preset a med
        LdMedInventory med = new LdMedInventory();
        med.setId("100");
        med.setBatchNo("N/A");
        med.setAmId(100);
        med.setAmMindose((short)1);
        med.setAmMaxdose((short)1);
        med.setAmDose((short)1);
        med.setAmMethod("v");
        med.setCreatedAt("1970-01-01 00:00:00");

        setDefaultMed(med);
        SetupDevice();
    }

    public Boolean isVarWeightOn() {
        return VarWeightOn;
    }

    public void setVarWeightOn(Boolean varWeightMode) {
        VarWeightOn = varWeightMode;
    }

    public Boolean getVarWeightNextDose() {
        return VarWeightNextDose;
    }

    public void setVarWeightNextDose(Boolean varWeightNextDose) {
        VarWeightNextDose = varWeightNextDose;
    }

    public Boolean getVarWeightDoseDone() {
        return VarWeightDoseDone;
    }

    public void setVarWeightDoseDone(Boolean varWeightDoseDone) {
        VarWeightDoseDone = varWeightDoseDone;
    }

    public AdapterSettings getAdapSettings() {
        return AdapSettings;
    }

    public boolean isDataReady() {
        return isDataReady;
    }

    private void SetupDevice() {
        int delay =500;//delay between packets
        Log.d("SetupDevice", state.toString());
        switch(state)
        {
            case STATE_SET_ID:
                byte[] data = new byte[1];
                data[0] = (byte)id;
                TcpPacket SetId_pck = new TcpPacket(deviceAddr.getIP(),(byte)0x02,(byte)0x0b,1,data,100);
                Connection.SendPacket(SetId_pck);
                break;

            case STATE_SET_TIME:
                Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int min = calendar.get(Calendar.MINUTE);
                int sec = calendar.get(Calendar.SECOND);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH)+1;
                int year = calendar.get(Calendar.YEAR);

                byte[] time = new byte[6];
                time[0] = (byte) hour;
                time[1] = (byte) min;
                time[2] = (byte) sec;
                time[3] = (byte) day;
                time[4] = (byte) month;
                time[5] = (byte) (year%100);
                TcpPacket SetTimePck = new TcpPacket(deviceAddr.getIP(),(byte)0x02, (byte)0x08, time.length, time,100);
                Connection.SendPacket(SetTimePck);
                break;

            case STATE_GET_SYS_INFO:

                byte[] version = new byte[2];

                // hard coded for the moment
                int majorVersion = 1;
                int minorVersion = 16;
                version[0] = (byte) majorVersion;
                version[1] = (byte) minorVersion;
                TcpPacket sysInfo_pck = new TcpPacket(deviceAddr.getIP(),(byte)0x02,(byte)0x09,version.length,version,ReadDelay);
                Connection.SendPacket(sysInfo_pck);
                break;

            case STATE_GET_MEM_INFO:
                TcpPacket MemInfo_pck = new TcpPacket(deviceAddr.getIP(),(byte)0x02,(byte)0x0A,0,null,ReadDelay);
                Connection.SendPacket(MemInfo_pck);
                break;

            case STATE_GET_ADAP_SETT:
                TcpPacket AdapSett_pck = new TcpPacket(deviceAddr.getIP(),(byte)0x04,(byte)0x0D,0,null,ReadDelay);
                Connection.SendPacket(AdapSett_pck);
                //delay = 2000;
                break;

            case STATE_SET_FARM_INFO:
                ByteBuffer farmInfo = ByteBuffer.allocate(7);
                farmInfo.order(ByteOrder.LITTLE_ENDIAN);
                //farmInfo.putInt(farmId); // this can no longer be used as farmId is not an int but a GUID string
                //TODO check if this protocol is used and if so we need a workaround for the GUID to send to the device
                farmInfo.putInt(0);
                TcpPacket pck = new TcpPacket(deviceAddr.getIP(),(byte)0x05, (byte)0x0E, farmInfo.capacity(), farmInfo.array(),ReadDelay);
                Connection.SendPacket(pck);
                //delay = 500;
                break;
            case STATE_DONE:
                isDataReady = true;
                break;
        }

        try {
            Thread.sleep(delay);
        }catch (Exception e) {
            Log.e("SetupDevice","error",e);
        }

        if(state.ordinal()< DevStates.STATE_DONE.ordinal()) {
            state = DevStates.values()[state.ordinal() + 1];
            SetupDevice();
        }
    }



    public static String bytesToHex(byte[] in, int len) {
        final StringBuilder builder = new StringBuilder();
        for(int i=0;i<len;i++) {
            builder.append(String.format("%02x", in[i]));
        }
        return builder.toString();
    }

    private void DisplayToast(String text){
        Intent i = new Intent("android.intent.action.AUTOMED").putExtra(AppConstants.HANDLER_TOAST_MSG, text);
        mAppContext.sendBroadcast(i);
    }

    private void DisplayAlert(String text){
        Intent i = new Intent("android.intent.action.AUTOMED").putExtra(AppConstants.HANDLER_ALERT_MSG, text);
        mAppContext.sendBroadcast(i);
    }


    private void processPacket(byte[] buffer, int len){
        // boolean flag for the new adapter state being in progress
        // we use this because the intial program phase for the adapter returns a state that makes us think the adapter is still blank (not already in progress)
        // if our 'new programming' code has been initiated then ignore the temptation to re-set the new adapter state to true
        if (WaitingForNewAdapterSetup == null) {
            WaitingForNewAdapterSetup = false;
        }

        TcpPacket pck = new TcpPacket(buffer, len);
        int pckId = pck.getId();
        switch (pckId){
            case 0x0102:// get ssid
            {
                Log.d("GET SSID 0102", bytesToHex(pck.getData(),pck.getDataLength()));
            }
            break;

            case 0x040B: // adapter state
            {
                byte[] res = pck.getData();
                byte state = res[0];
                byte error = res[1];
                int am_id = res[0] | res[1] << 8 | res[2] << 16 | res[3] << 24;
                Log.d("ADAPT STATE 040B", bytesToHex(pck.getData(),pck.getDataLength()));
                if(error == 0) {
                    switch (state) {
                        case 0://adapter not connected
                        {
                            DisplayToast("Adapter Not Connected");
                        }
                        break;

                        case 1:// adapter already configured
                        {
                            //update created at date
                            if( MedInventory != null ) {
                                AdapSettings.setCreatedAt(new String(MedInventory.getCreatedAt()));
                            }
                        }
                        break;

                        case 2:// new adapter
                        {
                            //update created at date
                            if( MedInventory != null && AdapSettings != null ) {
                                AdapSettings.setCreatedAt(new String(MedInventory.getCreatedAt()));
                            }

                            if (WaitingForNewAdapterSetup == false) {
                                NewAdapter = true;
                            }
                        }
                        break;
                    }
                }else{ // the device reported an error
                    String msg=new String();
                    switch(error){
                        case 1:
                        {
                            msg = "Adapter Size or Dose Method is not compatible with Selected Drug";
                            resetAdapter();
                        }break;

                        case 2:
                        {
                            msg = "Unable to write to eeprom";
                        }break;

                        case 3:
                        {
                            msg = "Wrong medication code\n"+"Adapter already configured with med:" + am_id;
                        }break;

                        case 4:
                        {
                            msg = "Unable to find Factory settings\n"+"Please check your adapter";
                        }break;
                    }
                    DisplayToast(msg);
                }

            }break;

            case 0x040D: // adapter settings
            {
                Log.d("ADAPTER SETT 040D", bytesToHex(pck.getData(), pck.getDataLength()));
                AdapterSettings adapterSettings = new AdapterSettings(pck);

                if( adapterSettings.isConnected() ) {
                    Log.d("CreatedAT ", adapterSettings.getCreatedAt());
                }
                NewAdapter = false;
                if(adapterSettings.getAmid()==0 && adapterSettings.isConnected())
                {
                    NewAdapter = true;
                    preConfigureAdapter(getDefaultMed(), mAppContext);
                }
                if(adapterSettings.isConnected()) {
                    AdapSettings = new AdapterSettings(pck);
                    boolean found=false;
                    DisplayAlert("Please pull the trigger to configure your adapter("+adapterSettings.getModelNum()+").");
                    /*if(!found)
                    {
                        DisplayConfMedication();
                    }*/
                }else{
                    DisplayToast("Adapter Disconnected");
                    AdapSettings = new AdapterSettings(pck);
                    if(VarWeightOn)
                    {
                        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                        btAdapter.disable();
                    }
                }
            }break;


            case 0x0209: // system info
            {
                byte[] res = pck.getData();
                Log.d("SYS INFO 0209", bytesToHex(pck.getData(),pck.getDataLength()));
                fwVer = res[1] | res[0] << 8;
            }break;

            case 0x020A: // Mem info
            {
                byte[] res = pck.getData();
                Log.d("MEM INFO 020A", bytesToHex(pck.getData(),pck.getDataLength()));
                TotalMemSize = byteArrayToInt(res,0);
                UsedMemSize = byteArrayToInt(res,4);
            }break;

            case 0x040C: // record treatment
            {
                Log.d("RECEIVE TREATMENT 040C", bytesToHex(pck.GetBuffer(), len));
                if (MedInventory == null) {
                    Log.d("MedInv NULL", "did not record dose");
                    DisplayToast("An Error Occurred: Treatment received from device with no medication configured.  Dose NOT Recorded!");

                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(mAppContext, notification);
                        r.play();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                } else {
                    UsedMemSize++;// update the used memory
                    setDosingMode(true);
                    DbHelper db = DbHelper.getInstance(mAppContext);
                    this.actionMode = true;

                    String checkCompleteStatus = bytesToHex(pck.GetBuffer(), len);
                    checkCompleteStatus = checkCompleteStatus.substring(9,10);

                    LdTreatment ldTreatment = new LdTreatment();
                    ldTreatment.setFarm(MedInventory.getFarm());

                    if(checkCompleteStatus.equals("2")){
                        ldTreatment.setIncomplete(1);
                    }
                    ldTreatment.setTrDate(AppMethods.getDateTime());
                    ldTreatment.setTrSerialno(deviceAddr.getMAC());
                    ldTreatment.setTrFirmware(getFwVerString());
                    if (AdapSettings != null) {
                        ldTreatment.setTrAmodno(AdapSettings.getModelNum());
                    }
                    ldTreatment.setTrBatchno(MedInventory.getBatchNo());


                    ldTreatment.setId(UUID.randomUUID().toString());

                    long tagID;
                    byte res[] = Arrays.copyOfRange(pck.getData(), 1, pck.getDataLength());

                    tagID = (long) res[0] & 0xFF;
                    tagID |= ((long) res[1] & 0xFF) << 8;
                    tagID |= ((long) res[2] & 0xFF) << 16;
                    tagID |= ((long) res[3] & 0xFF) << 24;
                    tagID |= ((long) res[4] & 0xFF) << 32;
                    tagID |= ((long) res[5] & 0xFF) << 40;

                    long NationaCode = tagID & 0xFF;
                    NationaCode |= (tagID >> 8 & 0xFF) << 8;
                    NationaCode |= (tagID >> 16 & 0xFF) << 16;
                    NationaCode |= (tagID >> 24 & 0xFF) << 24;
                    NationaCode |= (tagID >> 32 & 0x3F) << 32;

                    long CountryCode = (tagID >> 32 & 0xC0) >> 6;
                    CountryCode |= (tagID >> 40 & 0xFF) << 2;


                    // Either we are in Group RFID mode and we are the master, so we use what comes in from the device
                    // or we are not in Group RFID mode so we process as normal
                    String stag = String.format("%03d%012d", CountryCode, NationaCode);
                    Log.d("Treatment EID", stag);
                    ldTreatment.setAmbTemp(res[6]);
                    if (stag.isEmpty() || stag.contentEquals("000000000000000")) {
                        if (animalEID != null) {
                            ldTreatment.setLdLifedata(this.getanimalEID());
                        } else {
                            ldTreatment.setLdLifedata("000000000000000");
                        }
                    } else {
                        ldTreatment.setLdLifedata(stag);
                        this.setanimalEID(stag);
                    }

                    ldTreatment.setRefTreatmentsAmId(MedInventory.getAmId());
                    if (AdapSettings != null) {
                        ldTreatment.setTrSerialAdapter(String.valueOf(AdapSettings.getSerialNum()));
                    }
                    if (!this.getVarWeightNextDose() && MedInventory.getAmMethod() != 'm') {
                        ldTreatment.setWeight(this.getWeight());
                    } else {
                        ldTreatment.setWeight(0.0);
                    }

                    if(this.getanimalVID()!=null){
                        ldTreatment.setVid(this.getanimalVID());
                    }else{
                        ldTreatment.setVid("000");
                    }

                    ldTreatment.setTrReason(reason);

                    if(unit.length() > 0){
                        ldTreatment.setUnit(unit);
                    } else{
                        SharedPreferences shared_unit;
                        shared_unit = mAppContext.getSharedPreferences("unit", mAppContext.MODE_PRIVATE);
                        String unit_string = shared_unit.getString("unit_object",null);
                        String unit_object = new Gson().fromJson(unit_string,String.class);

                        ldTreatment.setUnit(unit_object);
                    }

                    if (getRemainingMed() != null) {
                        //Turn off the rfid as long as it is not mode 2
                        if(rfidMode!=2 && isRfidOn() == true){
                            TurnRfidOff();
                            try{
                                Thread.sleep(500);
                            } catch(Exception e){

                            }
                        }
                        //To monitor the incomplete status
                        if (ldTreatment.getIncomplete() > 0 && incompleteFlag <= 0)
                        {
                            incompleteFlag = ldTreatment.getIncomplete();
                        }
                        SetVarWeightMode();
                        double remainingDoseToAdminister = getRemainingMed().getAmDose();
                        if(remainingDoseToAdminister > getAdapSettings().getMaxDose()){
                            MedInventory.setAmDose(getAdapSettings().getMaxDose());
                            remainingDoseToAdminister -= getAdapSettings().getMaxDose();
                            LdMedInventory remMed = new LdMedInventory(MedInventory);
                            remMed.setAmDose(remainingDoseToAdminister);
                            setRemainingMed(remMed);
                            DisplayToast("Please continue dosing");
                        } else{
                            MedInventory.setAmDose(remainingDoseToAdminister);
                            setRemainingMed(null);
                            DisplayToast("Please continue dosing");
                        }
                        Log.d("DigistarRem 2","Dosing....");
                        Log.d("DigistarRem 3",String.valueOf(MedInventory.amDose));
                        ConfigureAdapter(MedInventory, true, mAppContext);
                    } else {
                        //Turn off the rfid as long as it is not mode 2
                        if(rfidMode!=2 && rfidMode!=1 && isRfidOn() == true){
                            TurnRfidOff();
                            try{
                                Thread.sleep(500);
                            } catch(Exception e){

                            }
                        }
                        String treat = new Gson().toJson(ldTreatment);
                        if(getTotalDose() > 0 && noMedMode == false){
                            ldTreatment.setTrDose(getTotalDose());
                        } else if(getTotalDose() <=0 && noMedMode == false){
                            ldTreatment.setTrDose(MedInventory.getAmDose());
                        } else{
                            ldTreatment.setTrDose(0);
                        }

                        if(ldTreatment.getIncomplete() <= 0)
                        {
                            ldTreatment.setIncomplete(incompleteFlag);
                        }

                        db.createLdTreatment(ldTreatment);

                        incompleteFlag = 0;
                        retreatedFlag = false;

                        if(returnStringToBTScale == true && BTdeviceName.equalsIgnoreCase("StockaID")){
                            StockaID sta = new StockaID();
                            sta.setAnimalID(this.getanimalEID());
                            sta.setAnimalWeigh(weight);
                            sta.setDose(ldTreatment.getTrDose());
                            sta.setDrugBatch(MedInventory.getBatchNo());
                            sta.setDrugName(MedInventory.getAmProduct());
                            sta.setTreatmentDate(AppMethods.getDateTime());
                            sta.setIncomplete(ldTreatment.getIncomplete());
                            sta.setAmID(MedInventory.getAmId());

                            bc.SendCmd(new Gson().toJson(sta));
                        }

                        if (VarWeightOn) {
                            VarWeightDoseDone = true;
                        }
                        setTotalDose(0.0);
                        setDosingMode(false);
                    }

                    if (rfidMode!=0) {
                        //  we want to keep the existing EID for all modes because of split doses
                    } else {
                        // we don't want to retain the RFID beyond this treatment, so clear animal ID
                        this.setanimalEID("000000000000000");
                    }
                    if(doseType.equals("weight")){
                        Intent i = new Intent("android.intent.action.AUTOMED").putExtra(AppConstants.HANDLER_CLEAR, true);
                        mAppContext.sendBroadcast(i);
                    }

                    Log.d("0x040c", "Finished processing");
                }
            }
            break;

            case 0x0410: // Med Conf Req
            {
                final Context finalAppContext = mAppContext;
                Log.d("MEDIC CONFIG REQ 0410", bytesToHex(pck.GetBuffer(), len));
                ByteBuffer b = ByteBuffer.wrap(pck.getData());
                b.order(ByteOrder.LITTLE_ENDIAN);

                int amID = b.getInt();
                int seconds = b.getInt();

                ConfigureAdapter(getDefaultMed(),false,mAppContext);
            }
            break;
        }
    }

    public static int byteArrayToInt(byte[] b, int offset) {
        return   b[3+offset] & 0xFF |
                (b[2+offset] & 0xFF) << 8 |
                (b[1+offset] & 0xFF) << 16 |
                (b[0+offset] & 0xFF) << 24;
    }

    public void Stop() {
        if (Connection != null) {
            //Connection.stop();
            Connection.interrupt();
            Connection = null;
        }
    }

    private ConnectedDevices(Parcel in) {
        deviceAddr = new ClientScanResult(in.readString(),in.readString(),in.readString());
        fwVer = in.readInt();
        id = in.readInt();
        state = (DevStates) in.readValue(DevStates.class.getClassLoader());
        isDataReady = in.readInt()== 1?true:false;
        rfidOn = in.readInt()== 1?true:false;
        TotalMemSize = in.readInt();
        UsedMemSize = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(deviceAddr.getIP());
        dest.writeString(deviceAddr.getMAC());
        dest.writeString(deviceAddr.getInterface());
        dest.writeInt(fwVer);
        dest.writeInt(id);
        dest.writeValue(state);
        dest.writeInt(isDataReady==true?1:0);
        dest.writeInt(rfidOn == true ? 1 : 0);
        dest.writeInt(TotalMemSize);
        dest.writeInt(UsedMemSize);
    }

    public boolean equals(Object obj){

        if((obj instanceof ConnectedDevices) == false){
            return false; //objects cant be equal
        }

        ConnectedDevices another = (ConnectedDevices) obj;

        return this.id == another.getId();

    }


    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ConnectedDevices> CREATOR = new Parcelable.Creator<ConnectedDevices>() {
        @Override
        public ConnectedDevices createFromParcel(Parcel in) {
            return new ConnectedDevices(in);
        }

        @Override
        public ConnectedDevices[] newArray(int size) {
            return new ConnectedDevices[size];
        }
    };
}