package io.automed.com.android_essential.model.environments.json;

/**
 * Created by harris on 4/07/17.
 */
public class Stockbook {
    private String EID;
    private String VID;
    private Boolean Reweigh;
    private String Weight;
    private Boolean Poll;

    public String getEID(){return EID;}
    public void setEID(String EID){ this.EID = EID;}

    public String getVID(){return VID;}
    public void setVID(String VID){ this.VID = VID;}

    public Boolean getReweigh(){return Reweigh;}
    public void setReweigh(Boolean Reweigh){ this.Reweigh = Reweigh;}

    public String getWeight(){return Weight;}
    public void setWeight(String Weight){ this.Weight = Weight;}

    public Boolean getPoll(){return Poll;}
    public void setPoll(Boolean Poll){ this.Poll = Poll;}
}
