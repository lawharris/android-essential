package io.automed.com.android_essential.wifi;

/**
 * this class associates and id to a MAC address.
 * it is used when detecting connected devices in order to keep the same id for the same device
 * especially in multiple disconnection reconnection events
 *
 * @author Mohamed Anis Messaoud <medanis.messaoud@gmail.com>
 */
public class DeviceID {
    private String MAC;
    private int id;

    public String getMAC() {
        return MAC;
    }

    public DeviceID(String mac, int id){
        this.MAC = mac;
        this.id = id;
    }
    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
