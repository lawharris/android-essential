package io.automed.com.android_essential.utils;

import android.support.design.widget.Snackbar;

public class AppConstants {

    //periodic events
    public static int PERIOD_UPDATE_TREATMENT = 3000;
    public static int PERIOD_NET_SCAN = 3000;
    public static int PERIOD_DB_SYNC = 600000;

    //
    public static int SYNC_MSG_DURATION = Snackbar.LENGTH_SHORT;


    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Connection types
    public static int CONNECTIVITY_BLUETOOTH = 1;
    public static int CONNECTIVITY_WIFI = 2;

    // Events
    public static String EVENT_INVENTORY = "Inventory";
    public static String EVENT_TREATMENT = "Treatment";

    // Alerts class
    public static String INTENT_DISPLAYERROR = "Display error";

    //medication types
    public enum MedTypes{
        MED_TYPE_UNKNOWN,
        MED_TYPE_FIXED_DOSE,
        MED_TYPE_VAR_AGE,
        MED_TYPE_VAR_WEIGHT
    }

    // ui handler msg types
    public enum MSGTypes{
        MSG_TYPE_TOAST,
        MSG_TYPE_WEIGHT_SELECT,
        MSG_TYPE_ALERT
    }
    public static String HANDLER_MSG_TYPE = "MsgType";
    public static String HANDLER_TOAST_MSG = "ToastMsg";
    public static String HANDLER_ALERT_MSG = "AlertMsg";
    public static String HANDLER_CLEAR = "ClearMsg";
    public static String HANDLER_AGE_MSG_MEDID = "AgeMedID";
    public static String HANDLER_AGE_MSG_DEVID = "AgeDevID";
}
