package io.automed.com.android_essential.model.object.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class LdTreatment {
    private String id;
    private double trDose;
    private String trReldate;
    private String trDate;
    private String trSerialno;
    private String trFirmware;
    private String trAmodno;
    private String trBatchno;
    private String ldId;
    private String user;
    private String farm;
    private int amId;
    private String amProduct;
    private int ambtemp;
    private String trSerialAdapter;
    private double   weight;
    private String vid;
    private int incomplete;
    private String reason;
    private String unit;

    public String getTrSerialAdapter() {
        return trSerialAdapter;
    }

    public void setTrSerialAdapter(String trSerialAdapter) {
        this.trSerialAdapter = trSerialAdapter;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setTrDose(double trDose){
        this.trDose = trDose;
    }
    public double getTrDose(){
        return this.trDose;
    }
    public void setTrReldate(String trRelDate){
        this.trReldate = trRelDate;
    }
    public String getTrReldate(){
        return this.trReldate;
    }
    public void setTrDate(String trDate){
        this.trDate = trDate;
    }
    public String getTrDate(){
        return this.trDate;
    }
    public void setTrSerialno(String trSerialNo){
        this.trSerialno = trSerialNo;
    }
    public String getTrSerialno(){
        return this.trSerialno;
    }
    public void setTrFirmware(String trFirmware){
        this.trFirmware = trFirmware;
    }
    public String getTrFirmware(){
        return this.trFirmware;
    }
    public void setTrAmodno(String trAModNo){
        this.trAmodno = trAModNo;
    }
    public String getTrAmodno(){
        return this.trAmodno;
    }
    public void setTrBatchno(String trBatchno){
        this.trBatchno = trBatchno;
    }
    public String getTrBatchno(){
        return this.trBatchno;
    }
    public void setLdLifedata(String tag){

        this.ldId =  new String(tag);
    }

    public int getAmbTemp() {
        return ambtemp;
    }

    public void setAmbTemp(int ambTemp) {
        ambtemp = ambTemp;
    }

    public String getLdLifedata(){
        return this.ldId;
    }
    public void setUser(String user){
        this.user = user;
    }
    public String getUser(){
        return this.user;
    }
    public void setFarm(String farm){
        this.farm = farm;
    }
    public String getFarm(){
        return this.farm;
    }
    public void setRefTreatmentsAmId(int amId){
        this.amId = amId;
    }
    public int getRefTreatmentsAmId(){
        return this.amId;
    }
    public void setAmProduct(String amProduct){
        this.amProduct = amProduct;
    }
    public String getAmProduct(){
        return this.amProduct;
    }

    public String getAnimalID(){

        if (getLdLifedata().equals("000000000000000"))
        {
            return "NO ID";
        }else {
            return getLdLifedata();
        }
    }

    public String getHumanTrDose(){
        return String.format("%.2f ml", getTrDose());//DecimalFormat("#.00").format(getTrDose()) + " ml";
    }

    public String getAnimalWeight(){

            return String.format("%.1f "+getUnit(), getWeight());//DecimalFormat("#.0").format(getWeight()) + " kg";

    }

    public String getTrReason() {
        return reason;
    }

    public void setTrReason(String reason) {
        this.reason = reason;
    }

    public int getIncomplete() {
        return incomplete;
    }

    public void setIncomplete(int incomplete) {
        this.incomplete = incomplete;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getHumanTrDoseFormattedForCountry(Context context){
        String unit_string = "";
        String unit_object = "";
        SharedPreferences shared_unit = context.getSharedPreferences("unit", context.MODE_PRIVATE);
        unit_string = shared_unit.getString("unit_object",null);
        unit_object = new Gson().fromJson(unit_string,String.class);
        if(unit_object!=null && unit_object.equalsIgnoreCase("lbs")){
            return String.format("%.2f cc", getTrDose());//DecimalFormat("#.00").format(getTrDose()) + " ml";
        }else{
            return String.format("%.2f ml", getTrDose());//DecimalFormat("#.00").format(getTrDose()) + " ml";
        }
    }
}
