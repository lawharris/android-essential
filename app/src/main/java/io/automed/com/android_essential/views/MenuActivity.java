package io.automed.com.android_essential.views;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import io.automed.com.android_essential.R;
import io.automed.com.android_essential.bluetooth.BluetoothConnection;
import io.automed.com.android_essential.database.DbHelper;
import io.automed.com.android_essential.model.environments.bluetooth.Gallagher_TSi2;
import io.automed.com.android_essential.model.environments.bluetooth.Gallagher_TW;
import io.automed.com.android_essential.model.environments.bluetooth.ID5000;
import io.automed.com.android_essential.model.environments.bluetooth.Ranger2100;
import io.automed.com.android_essential.model.environments.bluetooth.Ranger2100_Hospital;
import io.automed.com.android_essential.model.environments.bluetooth.Ranger2100_Induction;
import io.automed.com.android_essential.model.environments.bluetooth.RinR420;
import io.automed.com.android_essential.model.environments.bluetooth.Rinstrum_Scales;
import io.automed.com.android_essential.model.environments.bluetooth.Ruddweigh;
import io.automed.com.android_essential.model.environments.bluetooth.Series3000;
import io.automed.com.android_essential.model.environments.bluetooth.StockBook;
import io.automed.com.android_essential.model.environments.bluetooth.StockaId;
import io.automed.com.android_essential.model.environments.bluetooth.XR5000;
import io.automed.com.android_essential.model.environments.wifi.cardinal;
import io.automed.com.android_essential.model.environments.wifi.digiStar_ez2;
import io.automed.com.android_essential.model.environments.wifi.lbs;
import io.automed.com.android_essential.model.environments.wifi.rice_lake;
import io.automed.com.android_essential.model.environments.wifi.tronix;
import io.automed.com.android_essential.model.object.api.LdMedInventory;
import io.automed.com.android_essential.utils.AppConstants;
import io.automed.com.android_essential.utils.AppMethods;
import io.automed.com.android_essential.wifi.ClientScanResult;
import io.automed.com.android_essential.wifi.ConnectedDevices;
import io.automed.com.android_essential.views.adapters.RecordsAdapter;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

public class MenuActivity extends BaseActivity{
    public static final String[] SIGNAL_LEVEL = {"Poor", "Fair", "Good", "Excellent"};

    public enum BluetoothDeviceType {
        Scales,
        RFIDReader
    }
    private static ArrayList<ClientScanResult> OldScan = new ArrayList<ClientScanResult>();
    private static ArrayList<ClientScanResult> NewScan = new ArrayList<ClientScanResult>();

    private Handler handler = new Handler();
    private boolean isHandlerRunning = false;
    private Context mContext;
    private TextView tvRecCount;

    private BluetoothConnection Connection;
    private BluetoothConnection ConnectionRFID;
    static Boolean WeightListOn = false;
    static Boolean ChangeBottleListOn = false;
    static Boolean AlertListOn = false;
    boolean AdapConfigListOn = false;
    private String unit_string = "";
    private String unit_object = "";
    private String AIDA_weight="";
    private String AIDA_scale_type="";
    private String AIDAWeight_medID;
    private ConnectedDevices AIDAWeight_dev;
    private Boolean cardinalWeightChange = false;
    private Boolean cardinalStableWeight = false;
    private Boolean rinstrumWeightChange = false;
    private Boolean rinstrumStableWeight = false;
    private Boolean riceLakeWeightChange = false;
    private Boolean riceLakeStableWeight = false;
    private Boolean lbsWeightChange = false;
    private Boolean lbsStableWeight = false;
    private Boolean tronixWeightChange = false;
    private Boolean tronixStableWeight = false;
    private String tronixString = "";
    private Spinner sp_unit;
    private ArrayAdapter<String> adapter_unit;
    private ArrayList<String> arrayUnit;
    private BroadcastReceiver mReceiverType;
    private BroadcastReceiver mReceiverGeneral;
    private Boolean waitingForScaleToZeroBeforeNextAnimal = false;
    private Boolean overrideWeightWait = false;

    //Bluetooth scales
    private StockaId staId = null;
    private XR5000 xr5 = null;
    private ID5000 id5 = null;
    private Series3000 xr3 = null;
    private Ranger2100 r21 = null;
    private Ranger2100_Hospital r21H = null;
    private Ranger2100_Induction r21I = null;
    private Gallagher_TSi2 gTsi2 = null;
    private Gallagher_TW gTW = null;
    private RinR420 r420 = null;
    private Rinstrum_Scales rins = null;
    private StockBook stb = null;
    private Ruddweigh rudd = null;
    private String liquid_unit = "";
    private RecordsAdapter recordsAdapter;
    private Boolean appIsInGroupRFIDMode = false;
    private TourGuide mTourGuideHandler;

    //Wifi scales
    private digiStar_ez2 ds = null;
    private lbs lb = null;
    private cardinal car = null;
    private rice_lake rl = null;
    private tronix tr = null;

    private static final Map<Integer,Double> mStepsToMlMap;
    static
    {
        mStepsToMlMap = new HashMap<Integer, Double>();
        mStepsToMlMap.put(13, 3.0);
        mStepsToMlMap.put(25, 5.0);
        mStepsToMlMap.put(50, 10.0);
        mStepsToMlMap.put(100, 20.0); // inserted definition for testing
        mStepsToMlMap.put(150, 30.0);
    }

    private void initViews(){
        TextView tvConnect = findViewById(R.id.tv_connect);
        mTourGuideHandler = TourGuide.init(this).with(TourGuide.Technique.CLICK);
        mTourGuideHandler.setPointer(new Pointer());
        mTourGuideHandler.setToolTip(new ToolTip().setTitle("Welcome to automed").setDescription("Disclaimer: Run it at your own risk.").setGravity(Gravity.TOP));
        mTourGuideHandler.setOverlay(new Overlay());
        mTourGuideHandler.playOn(tvConnect);


        sp_unit = (Spinner) findViewById(R.id.spinner_unit);

        arrayUnit = new ArrayList<>();
        arrayUnit.add("kg");
        arrayUnit.add("lbs");

        adapter_unit =
                new ArrayAdapter<String>(this, R.layout.spinner_item, arrayUnit);
        adapter_unit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_unit.setAdapter(adapter_unit);
        sp_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                SharedPreferences.Editor editor = getSharedPreferences("unit", MODE_PRIVATE).edit();
                editor.putString("unit_object", new Gson().toJson(item));
                editor.commit();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        tvRecCount = (TextView) findViewById(R.id.tv_record_count);
        recordsAdapter = new RecordsAdapter(getApplicationContext());
        PeriodicListViewUpdate.run();
    }

    private Runnable PeriodicListViewUpdate = new Runnable() {
        @Override
        public void run() {
            DbHelper db = DbHelper.getInstance(getApplicationContext());
            loadTreatments();

            String unit_string = "";
            String unit_object = "";
            SharedPreferences shared_unit = getSharedPreferences("unit", MODE_PRIVATE);
            unit_string = shared_unit.getString("unit_object",null);
            Log.d("Unit",unit_object);
            unit_object = new Gson().fromJson(unit_string,String.class);
            if (unit_object!=null && unit_object.equalsIgnoreCase("lbs")){
                liquid_unit = " cc";
            }else{
                liquid_unit = " mL";
            }
            setLiquidUnit(liquid_unit);
            if(((TextView) findViewById(R.id.quickDisplay_dose_value)).getText().equals("0 mL") || ((TextView) findViewById(R.id.quickDisplay_dose_value)).getText().equals("0 cc")){
                ((TextView) findViewById(R.id.quickDisplay_dose_value)).setText("0" + liquid_unit);
            }
            // don't keep running the update function if the user has been logged out
            handler.postDelayed(this, AppConstants.PERIOD_UPDATE_TREATMENT);
        }
    };

    private void loadTreatments(){
        DbHelper db = DbHelper.getInstance(getApplicationContext());
        recordsAdapter.setItems(db.getLdTreatments());
        setTreatmentsText(recordsAdapter.getCount());
        recordsAdapter.notifyDataSetChanged();
    }

    public void setTreatmentsText(int count){
        tvRecCount.setText(String.valueOf(count));
    }

    public void StartHotSpot(View view){
        mTourGuideHandler.cleanUp();
        setContext(mContext);
        isHandlerRunning = getIsHandlerRunning();
        if(isHandlerRunning == false) {
            TextView ConnButt = (TextView) findViewById(R.id.tv_connect);
            ConnButt.setText("Disconnect");
            setIsHandlerRunning(true);
            PeriodicNetScan.run();
            Thread t=new Thread() {
                public void run() {
                    try {
                        sleep(1*60*1000);
                        // Wipe your valuable data here
                        turnOffScanWithoutFullCleaning();// disable scan
                        return;
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            };
            t.start();
        }else
        {
            TextView ConnButt = (TextView) findViewById(R.id.tv_connect);
            ConnButt.setText("Connect");
            ConnButt.setEnabled(false);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            setIsHandlerRunning(false);
            StopAccessPoint();
            ConnButt.setText("Connect");
            ConnButt.setEnabled(true);
        }
    }

    public void StopAccessPoint()  {
        DbHelper db = DbHelper.getInstance(getApplicationContext());
        db.clearLdTreatment();

        clearDisplays();

        // cancel bluetooth connection if active
        // JH 160720
        // this code will need to be disabled when the app supports multiple simultaneous device displays (where pressing 'disconnect' disables the individual device not the entire WiFi connection)
        if (Connection == null) {
            // no action as connection is already null
        } else {
            Log.d("Disconnecting Bluetooth", "");
            Connection.disconnectBluetooth();
        }

        for(ConnectedDevices cd:getConnDevices()){
            cd.Stop();
        }

        //Stop all environments as well
        if(xr5 != null){
            xr5.stopEnvironment();
            xr5 = null;
        }else if(staId!=null){
            staId.stopEnvironment();
            staId = null;
        }else if(xr3!=null){
            xr3.stopEnvironment();
            xr3 = null;
        }else if(r21!=null){
            r21.stopEnvironment();
            r21 = null;
        }else if(r21H!=null){
            r21H.stopEnvironment();
            r21H = null;
        }else if(r21I!=null){
            r21I.stopEnvironment();
            r21I = null;
        }else if(gTsi2!=null){
            gTsi2.stopEnvironment();
            gTsi2 = null;
        }else if(gTW!=null){
            gTW.stopEnvironment();
            gTW = null;
        }else if(r420!=null){
            r420.stopEnvironment();
            r420 = null;
        }else if(rins!=null){
            rins.stopEnvironment();
            rins = null;
        }else if(stb!=null){
            stb.stopEnvironment();
            stb = null;
        }else if(rudd!=null){
            rudd.stopEnvironment();
            rudd = null;
        }

        if(car!=null){
            car.stopEnvironment();
            car = null;
        } else if(ds!=null){
            ds.stopEnvironment();
            ds = null;
        } else if(lb!=null){
            lb.stopEnvironment();
            lb = null;
        } else if(rl!=null){
            rl.stopEnvironment();
            rl = null;
        } else if(tr!=null){
            tr.stopEnvironment();
            tr = null;
        }

        //ConnDevices.clear();
        // stop listening for changes to WiFi state
        //Stop wifi connection for android API < 8
        turnOffScan();
        reEnableWIFI(mContext);
    }


    private void DisplayDoseMethod(final String medID, final ConnectedDevices dev){
        Log.d("NewAdap",String.valueOf(WeightListOn));
        Log.d("NewAdap",String.valueOf(dev.getId()));
        if(!WeightListOn) {
            android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(mContext);
            try {
                Thread.sleep(2000);

            }catch(InterruptedException e)
            {
                Log.e("Th Enable","Th Enabled Err" + e);
            }

            //DevList.setIcon(R.drawable.ic_launcher);
            DevList.setTitle("Select Dose Input Method");
            DevList.setCancelable(false);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    mContext,
                    android.R.layout.select_dialog_singlechoice);

            arrayAdapter.add("Manual");
            arrayAdapter.add("Bluetooth Scale");
            arrayAdapter.add("Bluetooth Scale (Buffer)");
            arrayAdapter.add("WiFi Scale");
            DevList.setNegativeButton(
                    "cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            WeightListOn = false;
                        }
                    });


            DevList.setAdapter(
                    arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0: {
                                    int totalSleep = 0;
                                    int sleepDelay = 500;
                                    if ( dev != null ) {
                                        dev.setDoseType("fix");
                                        //Required a delay so that the device can retrieve the settings in time
                                        SelectManual(dev);
                                    }
                                }
                                break;

                                case 1: {
                                    if(dev!=null){
                                        dev.setDoseType("weight");
                                        SelectBluetooth(dev,false);
                                    }
                                }
                                break;

                                case 2:{
                                    if(dev!=null) {
                                        dev.setDoseType("weight");
                                        SelectBluetooth(dev,true);
                                    }
                                }
                                break;

                                case 3:{
                                    if(dev!=null) {
                                        dev.setDoseType("weight");
                                        SelectAIDA(dev);
                                    }
                                }
                                break;
                            }
                            WeightListOn = false;
                        }
                    });
            DevList.show();
            WeightListOn = true;
        }
    }

    private void SelectBluetooth(final ConnectedDevices dev, final Boolean bufferMode)
    {
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(mContext);

        DevList.setTitle("Select Device");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                mContext,
                android.R.layout.select_dialog_singlechoice);

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if( !mBluetoothAdapter.isEnabled() ) {
            mBluetoothAdapter.enable();
        }
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if(pairedDevices.size()==0)
        {
            try {
                Thread.sleep(2000);
            }catch(InterruptedException e)
            {
                Log.e("BT Enable","BT Enabled Err" + e);
                showToast("Bluetooth Initialisation Error - Please Retry",getApplicationContext());
                return;
            }

            if(pairedDevices.size()==0)
            {
                showToast("No Bluetooth Devices Paired",getApplicationContext());
                return;
            }

        }

        for(BluetoothDevice bt : pairedDevices) {
            if (shouldDisplayBluetoothDevice(BluetoothDeviceType.Scales, bt.getName())) {
                arrayAdapter.add(bt.getName());
            }
        }

        DevList.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        DevList.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String btDeviceName = arrayAdapter.getItem(which).trim();
                        Log.d("BT Adapter", arrayAdapter.getItem(which));

                        //Call eLynx class
                        if(btDeviceName.contentEquals("XR5000")) {
                            if(xr5 == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                xr5 = new XR5000(MenuActivity.this,Connection,mContext,getConnDevices(),bufferMode);
                                xr5.setConnectedDevices(dev);
                                xr5.setConfiguring(true);
                                xr5.start();
                                xr5.setWeightFromBluetooth();
                            } else{
                                xr5.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("ID5000")) {
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(id5 == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                id5 = new ID5000(MenuActivity.this,Connection,mContext,getConnDevices(),bufferMode);
                                id5.setConnectedDevices(dev);
                                id5.setConfiguring(true);
                                id5.start();
                                id5.setWeightFromBluetooth();
                            } else{
                                id5.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("Series 3000")){
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(xr3 == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                xr3 = new Series3000(MenuActivity.this,Connection,mContext,getConnDevices());
                                xr3.setConnectedDevices(dev);
                                xr3.start();
                            } else{
                                xr3.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("RANGER2100")){
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(r21 == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                r21 = new Ranger2100(MenuActivity.this,Connection,mContext,getConnDevices());
                                r21.setConnectedDevices(dev);
                                r21.start();
                            } else{
                                r21.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("RANGER2100 Hospital")){
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(r21H == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                r21H = new Ranger2100_Hospital(MenuActivity.this,Connection,mContext,getConnDevices());
                                r21H.setConnectedDevices(dev);
                                r21H.start();
                            } else{
                                r21H.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("RANGER2100 INDUCTION")){
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(r21I == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                r21I = new Ranger2100_Induction(MenuActivity.this,Connection,mContext,getConnDevices());
                                r21I.setConnectedDevices(dev);
                                r21I.start();
                            } else{
                                r21I.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("Gallagher-TSi2")){
                            if(gTsi2 == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                gTsi2 = new Gallagher_TSi2(MenuActivity.this,Connection,mContext,getConnDevices(), bufferMode);
                                gTsi2.setConnectedDevices(dev);
                                gTsi2.setConfiguring(true);
                                gTsi2.start();
                                gTsi2.setWeightFromBluetooth();
                            } else{
                                gTsi2.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contains("TW-1") || btDeviceName.contains("TW-3")){
                            if(gTW == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                gTW = new Gallagher_TW(MenuActivity.this,Connection,mContext,getConnDevices(), bufferMode);
                                gTW.setConnectedDevices(dev);
                                gTW.setConfiguring(true);
                                gTW.start();
                                gTW.setWeightFromBluetooth();
                            } else{
                                gTW.setConnectedDevices(dev);
                            }
                        }else if(btDeviceName.contentEquals("Rin R420") || btDeviceName.contentEquals("RinR420")){
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(r420 == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                r420 = new RinR420(MenuActivity.this,Connection,mContext,getConnDevices());
                                r420.setConnectedDevices(dev);
                                r420.start();
                            } else{
                                r420.setConnectedDevices(dev);
                            }
                        } else if(btDeviceName.contentEquals("Rinstrum Scales")){
                            if(bufferMode == true){
                                showAlert("Buffer mode is currently not available for this scale, switching to normal mode now.","Alert",MenuActivity.this);
                            }
                            if(rins == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                rins = new Rinstrum_Scales(MenuActivity.this,Connection,mContext,getConnDevices());
                                rins.setConnectedDevices(dev);
                                rins.start();
                            } else{
                                rins.setConnectedDevices(dev);
                            }
                        } /*else if(btDeviceName.contentEquals("Stockbook")){
                            if(stb == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                stb = new StockBook(MenuActivity.this,Connection,mContext,getConnDevices());
                                stb.setConnectedDevices(dev);
                                stb.start();
                            } else{
                                stb.setConnectedDevices(dev);
                            }
                        }*/ else if(btDeviceName.contentEquals("Ruddweigh")){
                            Log.d("Alert","Activating RuddWeigh");
                            if(rudd == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,true,"!",true);
                                Log.d("Alert","Activating in process");
                                rudd = new Ruddweigh(MenuActivity.this,Connection,mContext,getConnDevices(), bufferMode);
                                rudd.setConnectedDevices(dev);
                                rudd.setConfiguring(true);
                                rudd.start();
                                rudd.setWeightFromBluetooth();
                            } else{
                                rudd.setConnectedDevices(dev);
                            }
                        }/*else if(btDeviceName.contentEquals("StockaID")){
                            if(staId == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                staId = new StockaId(MenuActivity.this,Connection,mContext,getConnDevices());
                                staId.setConnectedDevices(dev);
                                staId.start();
                            } else{
                                staId.setConnectedDevices(dev);
                            }
                        } else{
                            if(stb == null){
                                if (Connection != null) {
                                    Log.d("Disconnecting Bluetooth", "");
                                    Connection.disconnectBluetooth();
                                }
                                Connection = new BluetoothConnection(arrayAdapter.getItem(which),mContext,false,"",true);
                                stb = new StockBook(MenuActivity.this,Connection,mContext,getConnDevices());
                                stb.setConnectedDevices(dev);
                                stb.start();
                            } else{
                                stb.setConnectedDevices(dev);
                            }
                            Log.d("Alert","It comes to else");
                        }*/
                    }
                });
        DevList.show();
    }

    public Boolean shouldDisplayBluetoothDevice(BluetoothDeviceType bluetoothDeviceType, String deviceName) {

        if (bluetoothDeviceType == BluetoothDeviceType.Scales) {
            //Since we return true for every device, unknown device will be handled by stockbook
            //This is temporarily for StockBook unnamed bluetooth dongle
            return true;
        } else if (bluetoothDeviceType == BluetoothDeviceType.RFIDReader) {
            if (deviceName.contains("Aleis")) {
                // if there's an Aleis in the device list then we have an RFID reader
                return true;
            } else if (deviceName.contains("SDL400S")) {  // also need to check for the SDL440S in future builds once the wand ID is confirmed
                // if there's an Shearwell in the device list then we have an RFID reader
                return true;
            } else if (deviceName.contains("GGL HR5")) {  // also need to check for the other Gallagher readers
                // if there's an Shearwell in the device list then we have an RFID reader
                return true;
            } else if (deviceName.contains("Serial Adaptor")) {  // used by Jindalee
                return true;
            } else if (deviceName.contains("Serial Adapter")) {  // used by Jindalee
                return true;
            } else if (deviceName.contains("Allflex RFID")) {  // Allflex RFID as used by Smithfield
                return true;
            } else if (deviceName.contains("XRS")){
                return true;
            } else if (deviceName.contains("XRS2")){
                return true;
            }
            return false;

        }
        return false;
    }

    private void SelectManual(final ConnectedDevices dev)
    {
        int min = dev.getAdapSettings().getMinDose();
        int max = dev.getAdapSettings().getMaxDose();

        RelativeLayout linearLayout = new RelativeLayout(mContext);
        final int maxAdapterDose = max;
        final EditText doseEditText = new EditText(mContext);
        doseEditText.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        doseEditText.addTextChangedListener( new TextWatcher() {

            public void beforeTextChanged(CharSequence a,int b,int c,int d) {
            }

            public void onTextChanged(CharSequence a,int b,int c,int d) {
            }

            public void afterTextChanged(Editable s) {
                try {
                    int val = Integer.parseInt(s.toString());
                    //Log.d("Check display val",String.valueOf(val));
                    if (val > maxAdapterDose * 2) {
                        doseEditText.setText(String.valueOf(maxAdapterDose*2));
                        doseEditText.setSelection(doseEditText.getText().length());
                        showToast("Dose cannot be larger than 2x maximum adapter dose.",getApplicationContext());
                    } else if (val < 0) {
                        s.replace(0, s.length(), "00.0", 0, 4);
                        showToast("Dose cannot be less than 0.",getApplicationContext());
                    }
                } catch (NumberFormatException ex) {
                    // Do something
                }
            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams doseValueParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        doseValueParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(doseEditText,doseValueParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Select the dose value");
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                try {
                                    dev.setWeight(0);
                                    ManualDoseConfig(Double.parseDouble(doseEditText.getText().toString()),dev);
                                } catch ( NumberFormatException nfe ) {
                                    Log.e("RecordsActivity","Unable to convert doseEditText from string to double");
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void ManualDoseConfig(double dose,ConnectedDevices dev)
    {
        LdMedInventory med;
        med = dev.getDefaultMed();
        med.setAmMaxdose((short)dev.getAdapSettings().getMaxDose());

        Double maximumAdapterDose = 1.00 * dev.getAdapSettings().getMaxDose();
        if (dose > maximumAdapterDose) {
            med.setAmDose(dose/2);
        } else {
            med.setAmDose(dose);
        }
        setDoseDisplay(String.valueOf(dose),dev.getId());
        dev.ConfigureAdapter(med, true, this);
    }

    private void SelectAIDA(final ConnectedDevices dev)
    {
        AIDAWeight_dev = dev;
        AIDAWeight_medID = dev.getDefaultMed().getId();
        dev.SetVarWeightMode();
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(mContext);
        DevList.setTitle("Select WIFI Scale");

        //TODO: Harcoded wifi scale head
        String [] scales = {"Cardinal", "Digi Star", "Digi Star Buffer", "Rice Lake 920i", "LBS", "Tronix"};
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                mContext,
                android.R.layout.select_dialog_singlechoice);

        for(String scale : scales) {
            arrayAdapter.add(scale);
        }

        DevList.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


        DevList.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String scaleName = arrayAdapter.getItem(which);
                        switch (scaleName){
                            case "Cardinal":
                                AIDA_scale_type = "cardinal";
                                if(car == null){
                                    car = new cardinal(MenuActivity.this, mContext, ConnDevices, false);
                                    car.setConnectedDevices(dev);
                                    car.setConfiguring(true);
                                    car.start();
                                } else{
                                    car.setConnectedDevices(dev);
                                }
                                break;
                            case "Digi Star":
                                AIDA_scale_type = "digi-star-ez2";
                                if(ds == null){
                                    ds = new digiStar_ez2(MenuActivity.this,mContext,ConnDevices,false);
                                    ds.setConnectedDevices(dev);
                                    ds.setConfiguring(true);
                                    ds.start();
                                } else{
                                    ds.setConnectedDevices(dev);
                                }
                                break;
                            case "Digi Star Buffer":
                                AIDA_scale_type = "digi-star-ez2-buffer";
                                if(ds == null){
                                    ds = new digiStar_ez2(MenuActivity.this,mContext,ConnDevices,true);
                                    ds.setConnectedDevices(dev);
                                    ds.setConfiguring(true);
                                    ds.start();
                                } else{
                                    ds.setConnectedDevices(dev);
                                }
                                break;
                            case "Rice Lake 920i":
                                AIDA_scale_type = "rice-lake-920-i";
                                if(rl == null){
                                    rl = new rice_lake(MenuActivity.this, mContext, ConnDevices, false);
                                    rl.setConnectedDevices(dev);
                                    rl.setConfiguring(true);
                                    rl.start();
                                } else{
                                    rl.setConnectedDevices(dev);
                                }
                                break;
                            case "LBS":
                                AIDA_scale_type = "lbs";
                                if(lb == null){
                                    lb = new lbs(MenuActivity.this,mContext, ConnDevices,false);
                                    lb.setConnectedDevices(dev);
                                    lb.setConfiguring(true);
                                    lb.start();
                                } else{
                                    lb.setConnectedDevices(dev);
                                }
                                break;
                            case "Tronix":
                                AIDA_scale_type = "tronix";
                                if(tr == null){
                                    tr = new tronix(MenuActivity.this, mContext, ConnDevices, false);
                                    tr.setConnectedDevices(dev);
                                    tr.setConfiguring(true);
                                    tr.start();
                                } else{
                                    tr.setConnectedDevices(dev);
                                }
                                break;
                            default:
                                AIDA_scale_type = "";
                                break;
                        }
                    }
                });
        DevList.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContext(mContext);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        WeightListOn = false;
        ChangeBottleListOn = false;
        AlertListOn = false;
        //request permissions

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.CHANGE_NETWORK_STATE, Manifest.permission.WRITE_SETTINGS, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }

        // register intent receiver to grab
        IntentFilter intentFilterType = new IntentFilter("android.intent.action.AUTOMED.type");
        IntentFilter intentFilterGeneral = new IntentFilter("android.intent.action.AUTOMED");

        mReceiverType = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                AppConstants.MSGTypes msgType;
                msgType = AppConstants.MSGTypes.values()[intent.getIntExtra(AppConstants.HANDLER_MSG_TYPE,0)];
                switch(msgType)
                {
                    case MSG_TYPE_WEIGHT_SELECT: {
                        closeAlert();
                        String medID = intent.getStringExtra(AppConstants.HANDLER_AGE_MSG_MEDID);
                        int devID = intent.getIntExtra(AppConstants.HANDLER_AGE_MSG_DEVID,0);
                        ConnectedDevices cd = null;
                        for (int i = 0; i < getConnDevices().size(); i++) {
                            if (getConnDevices().get(i).getId() == devID) {
                                cd = getConnDevices().get(i);
                                break;
                            }
                        }
                        DisplayDoseMethod(medID, cd);
                    }
                    break;
                }
            }
        };
        mReceiverGeneral = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                AppConstants.MSGTypes msgType;

                Boolean clearFlag = intent.getBooleanExtra(AppConstants.HANDLER_CLEAR,false);
                if(clearFlag==true){
                    clearDisplays();
                }


                String toast = intent.getStringExtra(AppConstants.HANDLER_TOAST_MSG);
                if(toast!=null && !toast.contentEquals("")){
                    showToast(toast,mContext);
                }

                String alert = intent.getStringExtra(AppConstants.HANDLER_ALERT_MSG);
                if(alert!=null && !alert.contentEquals("")){
                    showAlert(alert,"Info",mContext);
                }

                // EID intent
                String eid_receipt_message = intent.getStringExtra("EID_value");

                if (eid_receipt_message != null && !eid_receipt_message.contentEquals("")) {
                    Log.i("EID received", eid_receipt_message);
                    setDevEIDFromAIDA(eid_receipt_message);
                }

                String weight_aida = intent.getStringExtra("AIDA_weight");
                if (weight_aida != null && !weight_aida.contentEquals("")) {
                    if (AIDA_scale_type.equalsIgnoreCase("cardinal")) {
                        String cardinal_proper_weight = parseWeightForCardinal(weight_aida);
                        Log.d("Cardinal",cardinal_proper_weight);
                        Log.d("Cardinal status1",String.valueOf(cardinalStableWeight));
                        Log.d("Cardinal status2",String.valueOf(cardinalWeightChange));
                        if (cardinalStableWeight == true) {
                            if (car!=null && cardinalWeightChange == true && Double.parseDouble(cardinal_proper_weight) > 15) {
                                cardinalWeightChange = false;
                                car.setWeightFromAIDA(cardinal_proper_weight);
                            } else if(cardinalWeightChange == false && Double.parseDouble(cardinal_proper_weight) > 15){
                                cardinalWeightChange = false;
                            } else{
                                cardinalWeightChange = true;
                            }
                        }
                    } else if (AIDA_scale_type.equalsIgnoreCase("rice-lake-920-i")) {
                        String rice_lake_proper_weight = parseWeightForRiceLake(weight_aida);
                        if (riceLakeStableWeight == true) {
                            if (riceLakeWeightChange == true && rl!=null && Double.parseDouble(rice_lake_proper_weight) > 15) {
                                riceLakeWeightChange = false;
                                rl.setWeightFromAIDA(rice_lake_proper_weight);
                            } else if(riceLakeWeightChange == false && Double.parseDouble(rice_lake_proper_weight) > 15){
                                riceLakeWeightChange = false;
                            } else{
                                riceLakeWeightChange = true;
                            }
                        }
                    } else if (AIDA_scale_type.equalsIgnoreCase("tronix")) {
                        String tronix_proper_weight = parseWeightForTronix(weight_aida);
                        if (tronixStableWeight == true) {
                            if (tr!=null && tronixWeightChange == true && Double.parseDouble(tronix_proper_weight) > 15) {
                                tronixWeightChange = false;
                                tr.setWeightFromAIDA(tronix_proper_weight);
                            } else if(tronixWeightChange == false && Double.parseDouble(tronix_proper_weight) > 15){
                                tronixWeightChange = false;
                            } else{
                                tronixWeightChange = true;
                            }
                        }
                    } else if (AIDA_scale_type.equalsIgnoreCase("digi-star-ez2") || AIDA_scale_type.equalsIgnoreCase("digi-star-ez2-buffer")) {
                        String digi_star_proper_weight = parseWeightForDigiStarEz2(weight_aida);
                        if (ds != null && Double.parseDouble(digi_star_proper_weight) > 15) {
                            ds.setWeightFromAIDA(digi_star_proper_weight);
                        }
                    } else if (AIDA_scale_type.equalsIgnoreCase("lbs")) {
                        String lbs_proper_weight = parseWeightForLbs(weight_aida);
                        if (lbsStableWeight == true) {
                            if (lbsWeightChange == true && Double.parseDouble(lbs_proper_weight) > 15 && lb!=null) {
                                lbsWeightChange = false;
                                lb.setWeightFromAIDA(lbs_proper_weight);
                            } else if(lbsWeightChange == false && Double.parseDouble(lbs_proper_weight) > 15){
                                lbsWeightChange = false;
                            } else {
                                lbsWeightChange = true;
                            }
                        }
                    }
                }

                // clear display intent
                String clear_message = intent.getStringExtra("QuickDisplayClear");

                if (clear_message != null && !clear_message.contentEquals("")) {
                    Log.i("Clear received", clear_message);
                    //Same UI crash as progressbarsync, use a null check avoid crashes
                    setDoseDisplay("0");
                    setVIDDisplay("000");
                    setEIDDisplay("000000000000000");
                    setWeightDisplay("0");
                    // we only want to clear the EID display if we are not in group RFID mode
                    // reset code should be called by the ConnectedDevices class
                    //waitingForScaleToZeroBeforeNextAnimal = false;
                }
            }
        };
        //registering our receiver
        this.registerReceiver(mReceiverType, intentFilterType);
        this.registerReceiver(mReceiverGeneral,intentFilterGeneral);
    }

    private void setDevEIDFromAIDA(String eidFromAIDA) {
        // Group RFID uses this same function to send EIDs from the master device to all others
        String idNumbersOnly = eidFromAIDA.replaceAll("[^0-9\\.]+", "");

        ConnectedDevices dev = null;
        for (int i = 0; i < ConnDevices.size(); i++) {

            dev = ConnDevices.get(i);

            Log.d("Called AIDA","call 1");
            if (idNumbersOnly.length() > 15) {

                dev.setanimalEID(idNumbersOnly.substring(0, 15));
                Log.d("Animal ID Trimmed", idNumbersOnly.substring(0, 15));
                //((TextView) findViewById(R.id.quickDisplay_EID_value)).setText(idNumbersOnly.substring(0, 15));
                setEIDDisplay(idNumbersOnly.substring(0, 15));

            } else {
                dev.setanimalEID(idNumbersOnly);
                Log.d("Animal ID", idNumbersOnly);
                //((TextView) findViewById(R.id.quickDisplay_EID_value)).setText(idNumbersOnly);
                setEIDDisplay(idNumbersOnly);
            }
            //break;

        }
        //setEIDDisplay(idNumbersOnly);
        //setWeightDisplay(idNumbersOnly);
    }

    private String parseWeightForCardinal(String rawWeightString) {

        // a received weight packet should include the "GCZ" string if we are to treat it as a stable weight
        // if not then return "" as weight string which will not trigger any of the actual processing code
        // we can only handle kilogram and pound weights, so disregard if any other weight format is sent
        if((rawWeightString.indexOf(" LB G")!=-1 || rawWeightString.indexOf(" KG G")!=-1) && rawWeightString.indexOf(" MO")==-1 && rawWeightString.indexOf(" CZ")==-1) {

            int weightFirstSeparator = rawWeightString.indexOf("G");
            String rawWeight = rawWeightString.substring(0, weightFirstSeparator-2);  // the 3 characters preceding the 'G CZ' sequence are the units (plus a space), so remove them also
            String weightString = rawWeight.replaceAll("[^0-9\\.]+", "");

            // perform unit conversions if value includes lb
            if(rawWeightString.indexOf("LB")!=-1) {
                changeUnit("lbs");
            } else {
                changeUnit("kg");
            }
            cardinalStableWeight = true;
            return weightString;
        } else if(rawWeightString.indexOf(" MO")!=-1 && AIDAWeight_dev.getVarWeightNextDose() == false){
            cardinalStableWeight = false;
            cardinalWeightChange = true;
            return "0";
        } else{
            return "0"; // return empty string if format does not comply
        }
    }

    private String parseWeightForRiceLake(String rawWeightString) {
        if(rawWeightString.indexOf("LG")!=-1 && rawWeightString.indexOf("M")==-1 && rawWeightString.indexOf("Z")==-1) {

            int weightFirstSeparator = rawWeightString.indexOf("G");
            String rawWeight = rawWeightString.substring(0, weightFirstSeparator-1);  // the 3 characters preceding the 'G CZ' sequence are the units (plus a space), so remove them also
            String weightString = rawWeight.replaceAll("[^0-9\\.]+", "");

            riceLakeStableWeight = true;
            return weightString;
        } else if(rawWeightString.indexOf("M")!=-1 && AIDAWeight_dev.getVarWeightNextDose() == false){
            riceLakeStableWeight = false;
            riceLakeWeightChange = true;
            return "0";
        } else{
            return "0"; // return empty string if format does not comply
        }
    }

    private String parseWeightForTronix(String rawWeightString) {

        // a received weight packet should include the "GCZ" string if we are to treat it as a stable weight
        // if not then return "" as weight string which will not trigger any of the actual processing code
        // we can only handle kilogram and pound weights, so disregard if any other weight format is sent
        if(rawWeightString.indexOf("lb")!=-1 || rawWeightString.indexOf("kg")!=-1){
            String unitString = "";
            String weightString = "";
            Log.d("test Tronix ",tronixString);
            if(tronixString.indexOf("L:")!=-1){
                tronixString += rawWeightString;
                if(tronixString.indexOf("L:")!=-1){
                    unitString = tronixString;
                    weightString = tronixString.replaceAll("[^0-9\\.\\-]+", "");
                    weightString = weightString.trim();
                    // perform unit conversions if value includes lb
                    if(unitString.indexOf("lb")!=-1) {
                        changeUnit("lbs");
                    } else {
                        changeUnit("kg");
                    }
                    tronixStableWeight = true;
                    tronixString = "";
                    return weightString;
                } else{
                    tronixStableWeight = false;
                    tronixWeightChange = true;
                    return "0";
                }
            } else if (rawWeightString.indexOf("L:")!=-1){
                unitString = rawWeightString;
                weightString = rawWeightString.replaceAll("[^0-9\\.\\-]+", "");
                weightString = weightString.trim();
                // perform unit conversions if value includes lb
                if(unitString.indexOf("lb")!=-1) {
                    changeUnit("lbs");
                } else {
                    changeUnit("kg");
                }
                tronixStableWeight = true;
                tronixString = "";
                return weightString;
            } else {
                tronixStableWeight = false;
                tronixWeightChange = true;
                return "0";
            }
        } else{
            tronixStableWeight = false;
            tronixWeightChange = true;
            tronixString += rawWeightString;
            return "0";
        }
    }

    private String parseWeightForDigiStarEz2(String rawWeightString) {
        if(rawWeightString.indexOf("<STX>")!=-1 && rawWeightString.indexOf("<CR>")!=-1) {

            int weightFirstSeparator = rawWeightString.indexOf("<STX>");
            int weightSecondSeparator = rawWeightString.indexOf("<CR>");
            String rawWeight = rawWeightString.substring(weightFirstSeparator, weightSecondSeparator);
            String weightString = rawWeight.replaceAll("[^0-9\\.]+", "");

            // perform unit conversions if value includes lb
            if(rawWeightString.indexOf("$")!=-1){
                return weightString;
            }else{
                return "0.0";
            }
        } else {
            return ""; // return empty string if format does not comply
        }
    }

    private String parseWeightForLbs(String rawWeightString){
        if(rawWeightString.indexOf("lb")!=-1 || rawWeightString.indexOf("kg")!=-1) {
            int weightFirstSeparator = 0;
            rawWeightString = rawWeightString.replace(" ", "");
            if(rawWeightString.indexOf(".")==0){
                rawWeightString = rawWeightString.substring(1,rawWeightString.length());
            }
            if(rawWeightString.indexOf("lb")!=-1){
                weightFirstSeparator = rawWeightString.indexOf("lb");
                changeUnit("lbs");
            } else if (rawWeightString.indexOf("kg")!=-1){
                weightFirstSeparator = rawWeightString.indexOf("kg");
                changeUnit("kg");
            }

            String rawWeight = rawWeightString.substring(0, weightFirstSeparator);
            String weightString = rawWeight.replaceAll("[^0-9\\.\\-]+", "");
            if(weightString!=null && !weightString.isEmpty()){
                lbsStableWeight = true;
            } else{
                weightString = "0";
            }

            return weightString;
        } else{
            return "0"; // return empty string if format does not comply
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_menu;
    }

    @Override
    public void onStop() {
        try{
            this.unregisterReceiver(mReceiverType);
            this.unregisterReceiver(mReceiverGeneral);
        }catch (Exception e){

        }
        super.onStop();
    }

    public void PrimeAdapter(View view){
        if(getConnDevices().size()==0) {
            AppMethods.showDialog(this, null, getString(R.string.not_connected_msg));
            return;
        }
        if(getConnDevices().size()==1) {
            if(getConnDevices().get(0).getDefaultMed() !=null){
                getConnDevices().get(0).Prime();
            }else{
                showToast("Please configure the adapter D"+getConnDevices().get(0).getId(),this);
            }

        }else
            DisplayDeviceListForDeviceTreatment(0,this);
    }

    private void DisplayDeviceListForDeviceTreatment(final int caller, final Context context){
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(context);
        //DevList.setIcon(R.drawable.ic_launcher);
        DevList.setTitle("Select Device");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.select_dialog_singlechoice);
        final ArrayList<ConnectedDevices> ConnDevices = getConnDevices();
        for(ConnectedDevices dev:ConnDevices){
            arrayAdapter.add("Device " + dev.getId());
        }

        DevList.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        DevList.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConnectedDevices device;
                        device=ConnDevices.get(which);
                        if(caller == 0){
                            if(device.getDefaultMed() !=null){
                                device.Prime();
                            } else{
                                showToast("Please configure the adapter D"+device.getId(),context);
                            }
                        }else if(caller == 1){
                            device.TurnRfidOn();
                            if(appIsInGroupRFIDMode == false){
                                device.setIndividualRFIDMode(1);
                                device.TurnRfidOn();
                            }else{
                                //In group rfid mode
                                device.setIndividualRFIDMode(3);
                                device.TurnRfidOn();
                            }
                        }

                    }
                });
        DevList.show();
    }

    public void SelectBluetoothRFIDReader(View view)
    {
        SelectBluetoothRFIDLogic(mContext);
    }

    public void SelectBluetoothRFIDLogic(final Context context){
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(context);

        //DevList.setIcon(R.drawable.ic_launcher);
        DevList.setTitle("Select RFID Device");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.select_dialog_singlechoice){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                int textColor = textView.getText().toString().equals("Stop Scanning") ? R.color.am_orangedefault : R.color.am_white;
                textView.setBackgroundColor(ContextCompat.getColor(context,textColor));

                return textView;
            }
        };


        Boolean bluetoothRFIDReaderIsPaired = false;
        arrayAdapter.add("Stop Scanning");
        arrayAdapter.add("Device Reader (Individual)");
        arrayAdapter.add("Device Reader (Group)");

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter != null) {

            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if(pairedDevices.size()==0)
            {
                bluetoothRFIDReaderIsPaired = false;
            } else {
                for(BluetoothDevice bt : pairedDevices) {

                    if (shouldDisplayBluetoothDevice(BluetoothDeviceType.RFIDReader, bt.getName())) {
                        bluetoothRFIDReaderIsPaired = true;
                        arrayAdapter.add(bt.getName());
                    }
                }
            }

        }



        // if there's at least one RFID Device paired
        Boolean forceMenuDisplay = true;
        if (bluetoothRFIDReaderIsPaired || forceMenuDisplay) {


            DevList.setNegativeButton(
                    "cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });


            DevList.setAdapter(
                    arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:{
                                    ConnectedDevices dev = null;
                                    for (int i = 0; i < getConnDevices().size(); i++) {
                                        dev = getConnDevices().get(i);
                                        dev.TurnRfidOff();
                                        dev.setIndividualRFIDMode(0);
                                        dev.setanimalEID("000000000000000");
                                        setEIDDisplay("000000000000000");
                                    }
                                    break;
                                }
                                case 1: {
                                    appIsInGroupRFIDMode = false;
                                    // device reader was specified, so toggle state as normal
                                    if(getConnDevices().size()==0) {
                                        //AppMethods.showDialog(this, null, getString(R.string.not_connected_msg));
                                        showToast(getString(R.string.not_connected_msg),context);

                                        return;
                                    }
                                    if(getConnDevices().size()==1) {
                                        getConnDevices().get(0).setIndividualRFIDMode(1);
                                        getConnDevices().get(0).TurnRfidOn();
                                    }else {
                                        DisplayDeviceListForDeviceTreatment(1,context);
                                    }
                                }
                                break;
                                case 2: {
                                    // we're in group rfid mode, scanning new or re-scanning, so let's enable it
                                    appIsInGroupRFIDMode = true;

                                    if(getConnDevices().size()==0) {
                                        //AppMethods.showDialog(this, null, getString(R.string.not_connected_msg));
                                        showToast(getString(R.string.not_connected_msg),context);
                                        return;
                                    }
                                    if(getConnDevices().size()==1) {
                                        getConnDevices().get(0).setIndividualRFIDMode(3);
                                        getConnDevices().get(0).TurnRfidOn();
                                    }else {
                                        DisplayDeviceListForDeviceTreatment(1,context);
                                    }
                                }
                                break;

                                default: {
                                    // bluetooth reader was specified, so create a Bluetooth connection to the selected device
                                    DbHelper db = DbHelper.getInstance(context);
                                    Log.d("BT Adapter", arrayAdapter.getItem(which));
                                    final String btDeviceName = arrayAdapter.getItem(which);
                                    appIsInGroupRFIDMode = false;
                                    //RefEquipment equipment=db.getEquipmentByModel(arrayAdapter.getItem(which));

                                    ConnectionRFID = new BluetoothConnection(arrayAdapter.getItem(which), context,false,"",false);
                                    ConnectionRFID.setListener(new BluetoothConnection.OnReadListener() {

                                        @Override
                                        public void onRead(final byte[] response, final int len) {
                                            if (Looper.myLooper() == null) {
                                                Looper.prepare();
                                            }

                                            // JH 160920 bluetooth connection specifically for RFID readers only
                                            // scans do not affect device, just store the tag ID to record in the app
                                            // use equipment.model string to parse response from RFID reader and act accordingly

                                            // first set sleep timer boolean to false
                                            // the sleep timer is activated for particular devices that send incomplete bluetooth packets
                                            // sleeping for 500ms causes the bluetooth stack to receive the full packet before sending through to this listener

                                            Boolean connectionRequiresSleepPauseBetweenPackets = false;

                                            // then parse incoming bytes into string

                                            Log.d("onBluetoothReadLn", String.valueOf(len));
                                            String Str = new String(Arrays.copyOfRange(response, 0, len), StandardCharsets.UTF_8);
                                            //String Str = new String(Arrays.copyOfRange(response, 0, len), StandardCharsets.US_ASCII);

                                            Log.d("onBluetoothRead", Str);


                                            // now switch based upon type of device from equipment.model
                                            if (btDeviceName.contains("Aleis")) {

                                                // Aleis devices
                                                // must use btDeviceName an Contains as the device name includes the PIN and will vary between different device instances
                                                // setup parsing for RFID only
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {


                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");


                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        if (idNumbersOnly.length() > 15) {

                                                            dev.setanimalEID(idNumbersOnly.substring(7, idNumbersOnly.length()));
                                                            Log.d("Animal ID Trimmed", idNumbersOnly.substring(7, idNumbersOnly.length()));
                                                            setEIDDisplay(idNumbersOnly.substring(7, idNumbersOnly.length()));
                                                        } else {
                                                            dev.setanimalEID(idNumbersOnly);
                                                            Log.d("Animal ID", idNumbersOnly);
                                                            setEIDDisplay(idNumbersOnly);
                                                        }
                                                        //break;

                                                    }
                                                }
                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            } else if (btDeviceName.contains("SDL400S")) {

                                                // Aleis devices
                                                // must use btDeviceName an Contains as the device name includes the PIN and will vary between different device instances
                                                // setup parsing for RFID only
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {


                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");


                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        if (idNumbersOnly.length() > 15) {

                                                            dev.setanimalEID(idNumbersOnly.substring(0, 15));
                                                            Log.d("Animal ID Trimmed", idNumbersOnly.substring(0, 15));
                                                            //((TextView) findViewById(R.id.quickDisplay_EID_value)).setText(idNumbersOnly.substring(0, 15));
                                                            setEIDDisplay(idNumbersOnly.substring(0, 15));
                                                        } else {
                                                            dev.setanimalEID(idNumbersOnly);
                                                            Log.d("Animal ID", idNumbersOnly);
                                                            setEIDDisplay(idNumbersOnly);
                                                        }
                                                        //break;

                                                    }
                                                }
                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            } else if (btDeviceName.contains("GGL HR5")) {

                                                // Aleis devices
                                                // must use btDeviceName an Contains as the device name includes the PIN and will vary between different device instances
                                                // setup parsing for RFID only
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {


                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");


                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        if (idNumbersOnly.length() > 15) {

                                                            dev.setanimalEID(idNumbersOnly.substring(0, 15));
                                                            Log.d("Animal ID Trimmed", idNumbersOnly.substring(0, 15));
                                                            setEIDDisplay(idNumbersOnly.substring(0, 15));
                                                        } else {
                                                            dev.setanimalEID(idNumbersOnly);
                                                            Log.d("Animal ID", idNumbersOnly);
                                                            setEIDDisplay(idNumbersOnly);
                                                        }
                                                        //break;

                                                    }
                                                }

                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            } else if (btDeviceName.contains("Serial Adaptor")) {

                                                // Aleis devices
                                                // must use btDeviceName an Contains as the device name includes the PIN and will vary between different device instances
                                                // setup parsing for RFID only
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {


                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");
                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        Log.d("Set EID for Device", String.valueOf(i));
                                                        if (idNumbersOnly.length() > 15) {
                                                            // Jindalee RFID reader returns EID in substring 6-21
                                                            dev.setanimalEID(idNumbersOnly.substring(6,21));
                                                            Log.d("Animal ID Trimmed", idNumbersOnly.substring(6,21));
                                                            setEIDDisplay(idNumbersOnly.substring(0, 15));
                                                        } else {
                                                            dev.setanimalEID(idNumbersOnly);
                                                            Log.d("Animal ID", idNumbersOnly);
                                                            setEIDDisplay(idNumbersOnly);
                                                        }
                                                        //break;
                                                    }

                                                }
                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            }  else if (btDeviceName.contains("Allflex RFID")) {

                                                // Allflex RFID devices as oer Smithfield deployment
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {


                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");

                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        Log.d("Set EID for Device", String.valueOf(i));

                                                        dev.setanimalEID(idNumbersOnly);
                                                        Log.d("Animal ID", idNumbersOnly);
                                                        setEIDDisplay(idNumbersOnly);
                                                    }
                                                }

                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            }else if (btDeviceName.contains("XRS")) {
                                                // XRS RFID devices as oer Smithfield deployment
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {
                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");
                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        if(dev.isRfidOn() == true){
                                                            dev.TurnRfidOff();
                                                        }
                                                        Log.d("Set EID for Device", String.valueOf(i));

                                                        dev.setanimalEID(idNumbersOnly);
                                                        Log.d("Animal ID", idNumbersOnly);
                                                        setEIDDisplay(idNumbersOnly);
                                                    }
                                                }

                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            } else if (btDeviceName.contains("XRS2")) {

                                                // XRS RFID devices as oer Smithfield deployment
                                                if (Str.indexOf("\n") != -1)// if the ID has been returned
                                                {
                                                    String idNumbersOnly = Str.replaceAll("[^0-9\\.]+", "");
                                                    ConnectedDevices dev = null;
                                                    for (int i = 0; i < ConnDevices.size(); i++) {

                                                        dev = ConnDevices.get(i);
                                                        dev.setIndividualRFIDMode(2);
                                                        if(dev.isRfidOn() == true){
                                                            dev.TurnRfidOff();
                                                        }
                                                        Log.d("Set EID for Device", String.valueOf(i));

                                                        dev.setanimalEID(idNumbersOnly);
                                                        Log.d("Animal ID", idNumbersOnly);
                                                        setEIDDisplay(idNumbersOnly);
                                                    }
                                                }

                                                connectionRequiresSleepPauseBetweenPackets = true;
                                            } else {
                                                // discard packets if we don't know where they came from
                                            }

                                            // add small delay to try to catch full packets from BT
                                            if (connectionRequiresSleepPauseBetweenPackets) {
                                                try {
                                                    Thread.sleep(500);
                                                } catch (Exception e) {

                                                }
                                            }
                                        }

                                    });
                                    ConnectionRFID.start();
                                    break;
                                }

                            }
                        }
                    });
            DevList.show();


        } else {
            //if no bluetooth devices paired, just toggle device RFID reader
            if (ConnDevices.size() == 0) {
                AppMethods.showDialog(this, null, getString(R.string.not_connected_msg));
                return;
            }
            if (ConnDevices.size() == 1) {
                ConnDevices.get(0).TurnRfidOn();
            } else
                DisplayDeviceListForDeviceTreatment(1,this);
            return;
        }
    }

    public void requestReweigh(View view) {
        waitingForScaleToZeroBeforeNextAnimal = false;
        overrideWeightWait = true;
        clearDisplays();

        if(xr5!=null){
            xr5.resetWeight();
        } else if(staId!=null){
            staId.resetWeight();
        } else if(xr3!=null){
            xr3.resetWeight();
        } else if(r21!=null){
            r21.resetWeight();
        } else if(r21H!=null){
            r21H.resetWeight();
        } else if(r21I!=null){
            r21H.resetWeight();
        } else if(gTsi2!=null){
            gTsi2.resetWeight();
        } else if(gTW!=null){
            gTW.resetWeight();
        }else if(r420!=null){
            r420.resetWeight();
        } else if(rins!=null){
            rins.resetWeight();
        } else if(stb!=null){
            stb.resetWeight();
        } else if(rudd!=null){
            rudd.resetWeight();
        }

        if(ds!=null){
            ds.resetWeight();
        } else if(lb!=null){
            lb.resetWeight();
        } else if(car!=null){
            car.resetWeight();
        } else if(rl!=null){
            rl.resetWeight();
        } else if(tr!=null){
            tr.resetWeight();
        }

        cardinalWeightChange = true;
        riceLakeWeightChange = true;
        lbsWeightChange = true;
        tronixWeightChange = true;
    }

    public void requestReTreat(View view) {
        voidPreviousTreatmentAndReconfigureDose(getApplicationContext());
        if(xr5!=null){
            xr5.resetTreatment();
        } else if (rudd!=null){
            rudd.resetTreatment();
        } else if(gTsi2!=null){
            gTsi2.resetTreatment();
        } else if (gTW!=null){
            gTW.resetTreatment();
        }

        if(ds!=null){
            ds.resetTreatment();
        } else if(lb!=null){
            lb.resetTreatment();
        }
    }

    public void changeUnit(String unit){
        sp_unit.setSelection(adapter_unit.getPosition(unit));
        SharedPreferences.Editor editor = getSharedPreferences("unit", MODE_PRIVATE).edit();
        editor.putString("unit_object", new Gson().toJson(unit));
        editor.commit();
    }

    //generate screen menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    //menu items call commands
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void switchHotspotNetwork (MenuItem menuItem){
        configureHotspotNetwork();
    }

    @Override
    public void resetScan (MenuItem menuItem){
        resetScanLogic();
    }

    @Override
    public void aboutClicked(MenuItem menuItem){
        int version = 0;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(this);

        DevList.setTitle("About automed");
        DevList.setCancelable(false);
        DevList.setMessage("automed version: "+version);
        DevList.setPositiveButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        DevList.show();
    }

    public void resetScanLogic(){
        PeriodicNetScan.run();
        Thread t=new Thread() {
            public void run() {
                try {
                    sleep(1*60*1000);
                    // Wipe your valuable data here
                    turnOffScanWithoutFullCleaning();// disable scan
                    return;
                } catch (InterruptedException e) {
                    return;
                }
            }
        };
        t.start();
    }

    private void configureHotspotNetwork(){
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(this);

        DevList.setTitle("Select Hotspot SSID");
        DevList.setCancelable(false);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.select_dialog_singlechoice);

        arrayAdapter.add("AutoMed");
        arrayAdapter.add("automed1");
        arrayAdapter.add("automed2");
        DevList.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


        DevList.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: {
                                SharedPreferences.Editor editor = getSharedPreferences("network", MODE_PRIVATE).edit();
                                editor.putString("network_object", "AutoMed");
                                editor.commit();
                            }
                            break;

                            case 1: {
                                SharedPreferences.Editor editor = getSharedPreferences("network", MODE_PRIVATE).edit();
                                editor.putString("network_object", "automed1");
                                editor.commit();
                            }
                            break;

                            case 2: {
                                SharedPreferences.Editor editor = getSharedPreferences("network", MODE_PRIVATE).edit();
                                editor.putString("network_object", "automed2");
                                editor.commit();
                            }
                            break;
                        }

                        try{
                            StopAccessPoint();
                            Thread.sleep(500);
                            PeriodicNetScan.run();
                            Thread t=new Thread() {
                                public void run() {
                                    try {
                                        sleep(1*60*1000);
                                        // Wipe your valuable data here
                                        turnOffScanWithoutFullCleaning();// disable scan
                                        return;
                                    } catch (InterruptedException e) {
                                        return;
                                    }
                                }
                            };
                            t.start();
                        }catch(Exception ex){

                        }
                    }
                });
        DevList.show();
    }
}
