package io.automed.com.android_essential.model;

public class Response{
   	private String token;
    private int status_code;

 	public String getToken(){
		return this.token;
	}
	public void setToken(String token){
		this.token = token;
	}
    public int getStatusCode(){
        return this.status_code;
    }
    public void setStatusCode(int status_code){
        this.status_code = status_code;
    }
}
