package io.automed.com.android_essential.wifi;

/**
 * Created by jamesh on 22/11/16.
 */

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;
import io.automed.com.android_essential.database.DbHelper;
import io.automed.com.android_essential.model.Response;

public class AutomedAPIServer extends NanoHTTPD {


    private DbHelper db;
    public Context mContext;

    public AutomedAPIServer(int port) {
        super(port);
    }

    public AutomedAPIServer(String hostname, int port) {
        super(hostname, port);
    }

    @Override
    public Response serve(IHTTPSession session) {

        db = DbHelper.getInstance(mContext);

        String msg = "";
        Map<String, String> parms = session.getParms();
        String urlVars = session.getUri();
        Method urlMethod = session.getMethod();

        if (urlMethod == Method.GET) {
            if (urlVars.contains("treatments/last")) {
                Log.d("AIDA", "treatments/last");
                //String jsonLdTreat = new Gson().toJson(db.getLatestTreatmentForAIDA());
                //msg = jsonLdTreat;
            } else if (urlVars.contains("treatments/unsynced")) {
                Log.d("AIDA", "treatments/unsynced");
                //String jsonLdTreat = new Gson().toJson(db.getNotSyncedTreatmentsForAIDA());
               // msg = jsonLdTreat;
            } else if (urlVars.contains("treatments")) {
                Log.d("AIDA", "treatments");
                //String jsonLdTreat = new Gson().toJson(db.getAllTreatmentsForAIDA());
                //msg = jsonLdTreat;
            } else if (urlVars.contains("inventory")) {
                Log.d("AIDA", "inventory");
                //String jsonLdTreat = new Gson().toJson(db.getMedInventoriesForAIDA());
                //msg = jsonLdTreat;
            } else {
                Log.d("AIDA", "API ROOT");
                msg += "<html><body><h1>Hi, I'm AIDA</h1><br /><img src='https://images-na.ssl-images-amazon.com/images/M/MV5BMjExOTgzMjc3MF5BMl5BanBnXkFtZTgwNDY2NjcxMDI@._V1_.jpg'></body></html>";
            }

            //To shut down this function at the moment
            msg = "N/A";
        } else if (urlMethod == Method.PUT) {
            Log.d("AIDA", "API PUT");
            msg += "<html><body><h1>Hi, I'm AIDA</h1><br /><img src='https://s-media-cache-ak0.pinimg.com/originals/31/9f/d4/319fd4c266d2afe0c4dc60b3feecbd92.jpg'></body></html>";
        } else if (urlMethod == Method.POST) {
            Log.d("AIDA", "API POST");
            try {
                Log.d("AIDA Success","Parsing....");
                Map<String, String> files = new HashMap<String, String>();
                session.parseBody(files);
            } catch (Exception e) {
                Log.d("AIDA Error",e.toString());
                e.printStackTrace();
            }
            String postBody = session.getQueryParameterString();
            // or you can access the POST request's parameters
            Log.d("AIDA Reading 1",postBody);
            //String postParameter = session.getParms().get("parameter");
            //Log.d("AIDA Reading 2",postParameter);

            if (postBody != null) {
                // process body based upon URL

                Log.d("AIDA data", postBody);

                if (urlVars.contains("eid/")) {
                    // process data as an EID
                    Log.d("AIDA", "Processing EID");
                    if (urlVars.contains("allflex")) {
                        // code for allflex RFID reader
                        Log.d("AIDA", "Allflex RFID Reader");

                        Intent i = new Intent("android.intent.action.AUTOMED").putExtra("EID_value", postBody);
                        mContext.sendBroadcast(i);

                    } else if (urlVars.contains("aleis")) {
                        // code for allflex RFID reader
                        Log.d("AIDA", "Aleis RFID Reader");

                        String idNumbersOnly = postBody.replaceAll("[^0-9\\.]+", "");

                        Intent i = new Intent("android.intent.action.AUTOMED").putExtra("EID_value", idNumbersOnly.substring(7, 22));
                        mContext.sendBroadcast(i);

                    } else if (urlVars.contains("gallagher")) {
                        // code for Gallagher EID reader head
                        Log.d("AIDA", "Gallagher Reader");
                        String eidString = parseEIDForGallagher(postBody);
                        Log.d("AIDA Parsed EID", eidString);

                        Intent i = new Intent("android.intent.action.AUTOMED").putExtra("EID_value", eidString);
                        mContext.sendBroadcast(i);
                    }
                }else if(urlVars.contains("data/")){
                    if (urlVars.contains("weight")) {
                        // process data as a weight
                        Log.d("AIDA", "Processing Weight");
                        if (urlVars.contains("rinstrum")) {
                            // code for rinstrum scale head
                            Log.d("AIDA", "Rinstrum Scale Head");
                        } else if (urlVars.contains("cardinal")) {
                            // code for Cardinal scale head
                            Log.d("AIDA", "Cardinal Scale Head");
                            String weightString = parseWeightForCardinal(postBody);
                            Log.d("AIDA Parsed Weight", weightString);

                            Intent i = new Intent("android.intent.action.AUTOMED").putExtra("WEIGHT_value", weightString);
                            mContext.sendBroadcast(i);
                        } else if (urlVars.contains("gallagher")) {
                            // code for Gallagher scale head
                            Log.d("AIDA", "Gallagher Scale Head");
                            String weightString = parseWeightForGallagher(postBody);
                            Log.d("AIDA Parsed Weight", weightString);

                            Intent i = new Intent("android.intent.action.AUTOMED").putExtra("WEIGHT_value", weightString);
                            mContext.sendBroadcast(i);
                        } else{
                            Log.d("AIDA", "Processing Data Weight");
                            //Weight response = new Gson().fromJson(postBody, Weight.class);
                            //Log.d("AIDA", String.valueOf(response.getWeight()));

                            Intent i = new Intent("android.intent.action.AUTOMED").putExtra("AIDA_weight", postBody);
                            mContext.sendBroadcast(i);
                        }
                    }
                }

                if (urlVars.contains("/mute")) {
                    // if URL contains /mute command, return only empty set
                    msg = "";
                    Log.d("AIDA", "Suppressing Output");
                } else {
                    msg += postBody + "\n";
                }
            } else {
                msg = "Invalid Post Body";
                Log.d("AIDA", "Invalid");
            }


        }

        return newFixedLengthResponse( msg );
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private String parseWeightForGallagher(String rawWeightString) {

        // a received weight packet should include the "G.." string if we are to treat it as a stable weight
        // if not then return "" as weight string which will not trigger any of the actual processing code
        if(rawWeightString.indexOf("G..")!=-1) {

            int weightFirstSeparator = rawWeightString.indexOf("G..");
            String rawWeight = rawWeightString.substring(0, weightFirstSeparator);
            String weightString = rawWeight.replaceAll("[^0-9\\.]+", "");
            return weightString;
        } else {
            return "";
        }

    }

    private String parseWeightForCardinal(String rawWeightString) {

        // a received weight packet should include the "GCZ" string if we are to treat it as a stable weight
        // if not then return "" as weight string which will not trigger any of the actual processing code
        // we can only handle kilogram and pound weights, so disregard if any other weight format is sent
        if(rawWeightString.indexOf("LB G CZ")!=-1 || rawWeightString.indexOf("KG G CZ")!=-1) {

            int weightFirstSeparator = rawWeightString.indexOf("G CZ");
            String rawWeight = rawWeightString.substring(0, weightFirstSeparator-3);  // the 3 characters preceding the 'G CZ' sequence are the units (plus a space), so remove them also
            String weightString = rawWeight.replaceAll("[^0-9\\.]+", "");

            // perform unit conversions if value includes lb
            if(rawWeightString.indexOf("LB")!=-1) {
                try {
                    Double unconvertedWeight = Double.parseDouble(weightString);
                    Double convertedWeight = unconvertedWeight * 0.453592;
                    return String.format("%.1f", convertedWeight);
                } catch (NumberFormatException nfe) {
                    Log.e("AIDA Cardinal", "Unable to convert weight from string to double");
                    return "";
                }
            } else {
                return weightString;
            }
        } else {
            return ""; // return empty string if format does not comply
        }
    }

    private String parseEIDForGallagher(String rawEIDString) {

        // NOT TESTED
        // Based upon Gallagher Bluetooth code
        if(rawEIDString.indexOf("\\n")!=-1) {

            String idNumbersOnly = rawEIDString.replaceAll("[^0-9\\.]+", "");

            if (idNumbersOnly.length() > 15) {
                return idNumbersOnly.substring(0, 15);
            } else {
                return idNumbersOnly;
            }

        } else {
            return ""; // return empty string if format does not comply
        }
    }
}