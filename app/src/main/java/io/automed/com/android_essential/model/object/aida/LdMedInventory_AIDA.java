package io.automed.com.android_essential.model.object.aida;

public class LdMedInventory_AIDA {
    private long id;
    private double miQuantity;
    private String batchNo;
    private int amId;
    private String amProduct;
    private String expiry;

    public void setId(long id){
        this.id = id;
    }
    public long getId(){
        return this.id;
    }
    public void setMiQuantity(double miQuantity){
        this.miQuantity = miQuantity;
    }
    public double getMiQuantity(){
        return this.miQuantity;
    }
    public void setBatchNo(String batchNo){
        this.batchNo = batchNo;
    }
    public String getBatchNo(){
        return this.batchNo;
    }
    public void setAmId(int amId){
        this.amId = amId;
    }
    public int getAmId(){
        return this.amId;
    }
    public void setAmProduct(String amProduct){
        this.amProduct = amProduct;
    }
    public String getAmProduct(){
        return this.amProduct;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }



    public LdMedInventory_AIDA(){

    }

    public LdMedInventory_AIDA(LdMedInventory_AIDA SrcObj){
        this.id = SrcObj.getId();
        this.miQuantity = SrcObj.getMiQuantity();
        this.batchNo = SrcObj.getBatchNo();
        this.amId = SrcObj.getAmId();
        this.amProduct = SrcObj.getAmProduct();
        this.expiry = SrcObj.getExpiry();
    }
}
