package io.automed.com.android_essential.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.automed.com.android_essential.model.object.api.LdTreatment;
import io.automed.com.android_essential.utils.AppMethods;


public class DbHelper extends SQLiteOpenHelper {

    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 18;
    public static final String DATABASE_NAME = "automed";
    public static final String MAX_ID = "max_id";
    private Context mContext;
    private static DbHelper sInstance;
    private SharedPreferences shared_farm;
    private String farm_db = "";
    private String farm_string;
    private Ringtone r;

    public static synchronized DbHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LdTreatmentCP.CREATE_TABLE_LD_TREATMENTS);
        db.execSQL(CalibrationCP.CREATE_TABLE_CALIBRATION);
        db.execSQL(CalibrationCP.INSERT_TABLE_CALIBRATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("Upgrade Database","Upgrading");
        DisplayToast("We are upgrading your database.");
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            r = RingtoneManager.getRingtone(mContext, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.execSQL("DROP TABLE IF EXISTS " + CalibrationCP.TABLE_CALIBRATION);
        db.execSQL("DROP TABLE IF EXISTS " + LdTreatmentCP.TABLE_LD_TREATMENTS);
        onCreate(db);
    }

    public int getStepsForDoseAndAdapterType(double dose, String adapterType, double maximumAdapterReportedDose) {

        String adapterTypeKey = "";
        int required_steps = 0;

        if (adapterType.contentEquals("ad3")) {
            adapterTypeKey = CalibrationCP.KEY_AD_3;
        } else if (adapterType.contentEquals("ad5")) {
            adapterTypeKey = CalibrationCP.KEY_AD_5;

        } else if (adapterType.contentEquals("ad10")) {
            adapterTypeKey = CalibrationCP.KEY_AD_10;
        } else if (adapterType.contentEquals("ad15")) {
            adapterTypeKey = CalibrationCP.KEY_AD_15;
        } else if (adapterType.contentEquals("ad20")) {
            adapterTypeKey = CalibrationCP.KEY_AD_20;
        } else if (adapterType.contentEquals("ad25")) {
            adapterTypeKey = CalibrationCP.KEY_AD_25;
        } else if (adapterType.contentEquals("ad30")) {
            adapterTypeKey = CalibrationCP.KEY_AD_30;
        }

        if (adapterTypeKey.contentEquals("")) {
            // this adapter doesn't match a known key, so calculate based upon maximum dose reported in the function

            Log.d("getStepsForDose", "Unknown adapter using calculation instead");
            // calculate steps based upon max dose of adapter
            double  Fsteps = dose/maximumAdapterReportedDose*50;
            required_steps = (int) Math.round(Fsteps);


        } else {

            Log.d("getStepsForDose", "Using Calibration Table");


            String query = "SELECT MIN(STEP) AS REQUIRED_STEPS" +
                    " FROM " + CalibrationCP.TABLE_CALIBRATION +
                    " WHERE " + adapterTypeKey + " >= " + dose + ";";


            Log.d("Dose Query", query);

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(query, null);

            if (c.moveToFirst()) {
                //required_steps = c.getInt(c.getColumnIndex("REQUIRED_STEPS"));
                required_steps = c.getInt(0);
            }

            c.close();
        }
        //db.close();

        // ensure that no matter what, the maximum steps is 49
        if (required_steps > 49) { // if steps are calculated to be 50 or more (ie more than device can issue) then set back to 49.
            required_steps = 49;
        }


        return required_steps;
    }

    public void createLdTreatment(LdTreatment ld_treatment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if(ld_treatment!=null) {
            values.put(LdTreatmentCP.KEY_ID, ld_treatment.getId());
            values.put(LdTreatmentCP.KEY_TR_DOSE, String.valueOf(ld_treatment.getTrDose()));
            values.put(LdTreatmentCP.KEY_TR_RELDATE, ld_treatment.getTrReldate());
            values.put(LdTreatmentCP.KEY_TR_DATE, ld_treatment.getTrDate());
            values.put(LdTreatmentCP.KEY_TR_SERIALNO, ld_treatment.getTrSerialno());
            values.put(LdTreatmentCP.KEY_TR_FIRMWARE, ld_treatment.getTrFirmware());
            values.put(LdTreatmentCP.KEY_TR_AMODNO, ld_treatment.getTrAmodno());
            values.put(LdTreatmentCP.KEY_TR_BATCHNO, ld_treatment.getTrBatchno());
            values.put(LdTreatmentCP.KEY_LD_LIFEDATA, ld_treatment.getLdLifedata());
            values.put(LdTreatmentCP.KEY_USER, ld_treatment.getUser());
            values.put(LdTreatmentCP.KEY_FARM, ld_treatment.getFarm());
            values.put(LdTreatmentCP.KEY_AM_ID, ld_treatment.getRefTreatmentsAmId());
            values.put(LdTreatmentCP.KEY_AMB_TEMP, ld_treatment.getAmbTemp());
            values.put(LdTreatmentCP.KEY_SERIALNO_ADAP, ld_treatment.getTrSerialAdapter());
            values.put(LdTreatmentCP.KEY_WEIGHT, ld_treatment.getWeight());
            values.put(LdTreatmentCP.KEY_VID, ld_treatment.getVid());
            values.put(LdTreatmentCP.KEY_REASON, ld_treatment.getTrReason());
            values.put(LdTreatmentCP.KEY_UNIT,ld_treatment.getUnit());
            values.put(LdTreatmentCP.KEY_INCOMPLETE, ld_treatment.getIncomplete());
            db.insert(LdTreatmentCP.TABLE_LD_TREATMENTS, null, values);
        }
        //db.close();
    }

    public List<LdTreatment> getLatestTreatment() {
        List<LdTreatment> ldLdTreatmentList = new ArrayList<>();

        String query = "SELECT *" +
                " FROM "+ LdTreatmentCP.TABLE_LD_TREATMENTS +
                " ORDER BY " + LdTreatmentCP.KEY_TR_DATE + " DESC LIMIT 1";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                LdTreatment ldTreatment = new LdTreatment();
                ldTreatment.setId(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_ID))));
                ldTreatment.setTrDose(c.getDouble((c.getColumnIndex(LdTreatmentCP.KEY_TR_DOSE))));
                ldTreatment.setTrReldate(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_TR_RELDATE))));
                ldTreatment.setTrDate(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_TR_DATE))));
                ldTreatment.setTrSerialno(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_SERIALNO)));
                ldTreatment.setTrFirmware(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_FIRMWARE)));
                ldTreatment.setTrAmodno(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_AMODNO)));
                ldTreatment.setTrBatchno(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_BATCHNO)));
                ldTreatment.setLdLifedata(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_LD_LIFEDATA)));
                ldTreatment.setUser(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_USER)));
                ldTreatment.setFarm(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_FARM)));
                ldTreatment.setRefTreatmentsAmId(c.getInt((c.getColumnIndex(LdTreatmentCP.KEY_AM_ID))));
                ldTreatment.setAmbTemp(c.getInt((c.getColumnIndex(LdTreatmentCP.KEY_AMB_TEMP))));
                ldTreatment.setTrSerialAdapter(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_SERIALNO_ADAP))));
                ldTreatment.setWeight(c.getFloat((c.getColumnIndex(LdTreatmentCP.KEY_WEIGHT))));
                ldTreatment.setVid(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_VID))));
                ldTreatment.setTrReason(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_REASON)));
                ldTreatment.setUnit(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_UNIT)));
                ldTreatment.setIncomplete(c.getInt(c.getColumnIndex(LdTreatmentCP.KEY_INCOMPLETE)));
                ldLdTreatmentList.add(ldTreatment);
            } while (c.moveToNext());
        }

        c.close();
        //db.close();
        return ldLdTreatmentList;
    }

    public void updateLdTreatmentIncompleteStatus(String id,int status){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(LdTreatmentCP.KEY_INCOMPLETE,status);
        db.update(LdTreatmentCP.TABLE_LD_TREATMENTS,cv,LdTreatmentCP.KEY_ID+"=?",new String[]{id});
    }

    public void clearLdTreatment(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(LdTreatmentCP.TABLE_LD_TREATMENTS,null,null);
    }

    public List<LdTreatment> getLdTreatments() {
        List<LdTreatment> ldTreatments = new ArrayList<>();
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);


        String query = "SELECT *" +
                " FROM "+ LdTreatmentCP.TABLE_LD_TREATMENTS +" lt"+
                " ORDER BY lt."+ LdTreatmentCP.KEY_TR_DATE+ " DESC";

        //Log.d("REFRESH QUERY", query);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                LdTreatment ldTreatment = new LdTreatment();
                ldTreatment.setId(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_ID))));
                ldTreatment.setTrDose(c.getDouble((c.getColumnIndex(LdTreatmentCP.KEY_TR_DOSE))));
                ldTreatment.setTrReldate(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_TR_RELDATE))));
                ldTreatment.setTrDate(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_TR_DATE))));
                ldTreatment.setTrSerialno(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_SERIALNO)));
                ldTreatment.setTrFirmware(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_FIRMWARE)));
                ldTreatment.setTrAmodno(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_AMODNO)));
                ldTreatment.setTrBatchno(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_TR_BATCHNO)));
                ldTreatment.setLdLifedata(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_LD_LIFEDATA)));
                ldTreatment.setUser(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_USER)));
                ldTreatment.setFarm(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_FARM)));
                ldTreatment.setRefTreatmentsAmId(c.getInt((c.getColumnIndex(LdTreatmentCP.KEY_AM_ID))));
                ldTreatment.setWeight(c.getDouble((c.getColumnIndex(LdTreatmentCP.KEY_WEIGHT))));
                ldTreatment.setVid(c.getString((c.getColumnIndex(LdTreatmentCP.KEY_VID))));
                ldTreatment.setTrReason(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_REASON)));
                ldTreatment.setUnit(c.getString(c.getColumnIndex(LdTreatmentCP.KEY_UNIT)));
                ldTreatment.setIncomplete(c.getInt(c.getColumnIndex(LdTreatmentCP.KEY_INCOMPLETE)));
                ldTreatments.add(ldTreatment);
            } while (c.moveToNext());
        }

        c.close();
        //db.close();
        return ldTreatments;
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    private void DisplayToast(String text){
        Toast.makeText(mContext,text, Toast.LENGTH_LONG).show();
    }
}