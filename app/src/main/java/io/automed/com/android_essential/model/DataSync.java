package io.automed.com.android_essential.model;

public class DataSync {
	private boolean isManual;
   	private boolean refTreatments = true;
	private boolean ldJobHistory = true;
	private boolean ldMedInventory = true;
	private boolean ldTreatment = true;
	private boolean refEquipments = true;
	private boolean calibrations = true;
	private boolean incompleteDoses = true;

	public void setIsManual(boolean isManual){
		this.isManual = isManual;
	}
	public boolean isManual(){
		return this.isManual;
	}

	public void setRefTreatments(boolean refTreatments){
		this.refTreatments = refTreatments;
	}
	public boolean getRefTreatments(){
		return this.refTreatments;
	}
	public void setLdJobHistory(boolean ldJobHistory){
		this.ldJobHistory = ldJobHistory;
	}
	public boolean getLdJobHistory(){
		return this.ldJobHistory;
	}
	public void setLdMedInventory(boolean ldMedInventory){
		this.ldMedInventory = ldMedInventory;
	}
	public boolean getLdMedInventory(){
		return this.ldMedInventory;
	}
	public void setLdTreatment(boolean ldTreatment){
		this.ldTreatment = ldTreatment;
	}
	public boolean getLdTreatment(){
		return this.ldTreatment;
	}

	public boolean getRefEquipments() {
		return refEquipments;
	}

	public void setRefEquipments(boolean refEquipments) {
		this.refEquipments = refEquipments;
	}


	public boolean getCalibrations() {
		return calibrations;
	}

	public void setCalibrations(boolean calibrations) {
		this.calibrations = calibrations;
	}


	public boolean getIncompleteDoses() {
		return incompleteDoses;
	}

	public void setIncompleteDoses(boolean incompleteDoses) {
		this.incompleteDoses = incompleteDoses;
	}

	public boolean syncSuccessful(){
		return refTreatments && ldJobHistory
				&& ldMedInventory && ldTreatment;
	}
}
