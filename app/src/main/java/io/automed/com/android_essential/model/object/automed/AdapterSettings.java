package io.automed.com.android_essential.model.object.automed;

import android.util.Log;

import io.automed.com.android_essential.utils.AppMethods;
import io.automed.com.android_essential.wifi.TcpPacket;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class AdapterSettings {
    boolean Connected;
    private char Method;
    int Feed;
    private boolean NeedleGuard;
    int amid;
    int MinDose;
    int MaxDose;
    String ModelNum;
    int SerialNum;
    String CreatedAt;


    public AdapterSettings(TcpPacket packet){
        ByteBuffer b = ByteBuffer.wrap(packet.getData());
        b.order(ByteOrder.LITTLE_ENDIAN);
        Connected = b.get()==1;

        if(Connected) {
            Method = (char) b.get();
            Feed = b.get();
            NeedleGuard = (b.get() == 1);
            amid = b.getInt();
            MinDose = b.getShort();
            MaxDose = b.getShort();

            ModelNum = new String();
            ModelNum += Method;

            if (NeedleGuard)
                ModelNum += "NG";
            if (Feed == 0)
                ModelNum += "L-";
            if (Feed == 1)
                ModelNum += "BF-";

            ModelNum += MaxDose;

            SerialNum = b.getInt();
            CreatedAt = AppMethods.secondsToDateTime(b.getInt(),"yyyy-MM-dd HH:mm:ss");

        }
    }


    public int getSerialNum() {
        return SerialNum;
    }

    public void setSerialNum(int serialNum) {
        SerialNum = serialNum;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String CreatedAt) {
        this.CreatedAt = CreatedAt;
    }


    public char getMethod() {
        return Method;
    }

    public void setMethod(char method) {
        Method = method;
    }

    public int getFeed() {
        return Feed;
    }

    public void setFeed(int feed) {
        Feed = feed;
    }

    public boolean isNeedleGuard() {
        return NeedleGuard;
    }

    public void setNeedleGuard(boolean needleGuard) {
        NeedleGuard = needleGuard;
    }

    public int getAmid() {
        return amid;
    }

    public void setAmid(int amid) {
        this.amid = amid;
    }

    public int getMinDose() {
        return MinDose;
    }

    public void setMinDose(int minDose) {
        MinDose = minDose;
    }

    public int getMaxDose() {
        return MaxDose;
    }

    public void setMaxDose(int maxDose) {
        MaxDose = maxDose;
    }

    public String getModelNum() {
        return ModelNum;
    }


    public String getAdapterTypeString() {

        String typeString = "Unknown";

        Log.d("getAdapterType", String.valueOf(MaxDose));

        if (MaxDose == 3) {
            typeString = "ad3";
        } else if (MaxDose == 5) {
            typeString = "ad5";
        } else if (MaxDose == 10) {
            typeString = "ad10";
        } else if (MaxDose == 15) {
            typeString = "ad15";
        } else if (MaxDose == 20) {
            typeString = "ad20";
        } else if (MaxDose == 25) {
            typeString = "ad25";
        } else if (MaxDose == 30) {
            typeString = "ad30";
        }

        return typeString;
    }

    public void setModelNum(String modelNum) {
        ModelNum = modelNum;
    }

    public boolean isConnected() {
        return Connected;
    }
}
