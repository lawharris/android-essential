package io.automed.com.android_essential.model.environments.bluetooth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import io.automed.com.android_essential.bluetooth.BluetoothConnection;
import io.automed.com.android_essential.model.object.api.LdMedInventory;
import io.automed.com.android_essential.views.MenuActivity;
import io.automed.com.android_essential.wifi.ConnectedDevices;
import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by harris on 30/01/18.
 */
public class Gallagher_TSi2 extends Thread {
    public interface OnReadListener {
        void onRead(byte[] response, int len);
    }
    private BluetoothConnection Connection;
    private Context mAppContext;
    private ArrayList<ConnectedDevices> ConnDevices;
    private ArrayList<ConnectedDevices> SelectedDevices;
    private Boolean waitingForScaleToZeroBeforeNextAnimal = false;
    private Boolean overrideWeightWait = true;
    private String unit_string = "";
    private String unit_object = "";
    private OnReadListener listener = null;

    private boolean retreating = false;

    private ArrayList<Double> weight_array;
    private ArrayList<String> eid_array;
    private ArrayList<String> vid_array;
    private Boolean configuring = false;
    private Boolean doseStatusNextDone = false;
    private double weight = -999.0;
    private String eid = "000000000000000";
    private String vid = "000";
    private Boolean nextDoseString = false;
    private Boolean NewWeightListOn = false;
    private Boolean NextWeightListOn = true;
    private Handler mHandler;
    private Boolean bufferMode = false;
    private double previous_weight = 0.0;
    private String previous_eid = "000000000000000";
    private String previous_vid = "000";
    private MenuActivity mAct;
    private String BluetoothJsonString = "";

    public Gallagher_TSi2(MenuActivity mAct, BluetoothConnection Connection, Context AppContext, ArrayList<ConnectedDevices> ConnDevices, Boolean bufferMode){
        this.Connection = Connection;
        mAppContext = AppContext;
        this.ConnDevices = ConnDevices;
        SelectedDevices = new ArrayList<>();
        weight_array = new ArrayList<>();
        eid_array = new ArrayList<>();
        vid_array = new ArrayList<>();
        mHandler = new Handler(mAppContext.getMainLooper());
        this.bufferMode = bufferMode;
        this.mAct = mAct;
    }

    public void setMilPerKg(final ConnectedDevices dev)
    {
        int max = dev.getAdapSettings().getMaxDose();

        RelativeLayout linearLayout = new RelativeLayout(mAppContext);
        final int maxAdapterDose = max;
        final EditText doseEditText = new EditText(mAppContext);
        doseEditText.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        doseEditText.addTextChangedListener( new TextWatcher() {

            public void beforeTextChanged(CharSequence a,int b,int c,int d) {
            }

            public void onTextChanged(CharSequence a,int b,int c,int d) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        RelativeLayout.LayoutParams doseValueParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        doseValueParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        linearLayout.setLayoutParams(params);
        linearLayout.addView(doseEditText,doseValueParams);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mAppContext);
        alertDialogBuilder.setTitle("Select 1 mL per kg value for D"+dev.getId());
        alertDialogBuilder.setView(linearLayout);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                try {
                                    dev.setWeightDose(Double.parseDouble(doseEditText.getText().toString()));
                                } catch ( NumberFormatException nfe ) {
                                    Log.e("RecordsActivity","Unable to convert doseEditText from string to double");
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setConnectedDevices(){
        if(SelectedDevices.size()>0){
            SelectedDevices.clear();
        }
        android.support.v7.app.AlertDialog.Builder DevList = new android.support.v7.app.AlertDialog.Builder(mAppContext);
        DevList.setTitle("Select Applicators");
        String[] devicesName = new String[ConnDevices.size()];
        final boolean[] selectedDevices = new boolean[ConnDevices.size()];
        for(int i = 0; i < devicesName.length; i++) {
            devicesName[i] = "D"+ String.valueOf(ConnDevices.get(i).getId());
            selectedDevices[i] = false;
            setMilPerKg(ConnDevices.get(i));
        }
        DevList.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        DevList.setMultiChoiceItems(
                devicesName,
                selectedDevices,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        SelectedDevices.add(ConnDevices.get(indexSelected));
                        //To initialize the device for receiving a weight
                        ConnDevices.get(indexSelected).setVarWeightDoseDone(true);
                    }
                }).setPositiveButton("Ok",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int ii) {

            }
        }).setCancelable(false).create();

        DevList.show();
    }
    public void setConnectedDevices(ConnectedDevices dev){
        boolean contains = SelectedDevices.contains(dev);
        if(contains == false){
            SelectedDevices.add(dev);
            //To initialize the device for receiving a weight
            dev.setVarWeightDoseDone(true);
            setMilPerKg(dev);
        }
    }
    public void setWeightFromBluetooth(){
        Connection.setListener(new BluetoothConnection.OnReadListener() {
            @Override
            public void onRead(final byte[] response, final int len) {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }

                Log.d("onBluetoothReadLn", String.valueOf(len));
                String Str = new String(Arrays.copyOfRange(response, 0, len), StandardCharsets.UTF_8);
                BluetoothJsonString+=Str;
                if(BluetoothJsonString.indexOf(",kg")!=-1 || BluetoothJsonString.indexOf(",lb")!=-1)// a received weight packet should include the ",kg" string
                {
                    int EIDFirstSeparator = BluetoothJsonString.indexOf(",");
                    String rawEID = BluetoothJsonString.substring(0, EIDFirstSeparator);
                    String EIDString = rawEID.replaceAll("[^0-9\\.]+", "");

                    String firstStripString = BluetoothJsonString.substring(EIDFirstSeparator + 1); // string from the EID onwards
                    int secondSeparator = firstStripString.indexOf(",");
                    String rawVID = firstStripString.substring(0, secondSeparator);
                    String VIDString = rawVID.replaceAll("[^0-9\\.]+", "");

                    String secondStripString = firstStripString.substring(secondSeparator + 1);
                    int weightSeparator = -1;
                    if (BluetoothJsonString.indexOf(",lb") != -1) {
                        weightSeparator = secondStripString.indexOf(",lb");
                        mHandler.post(new Runnable() {
                            public void run() {
                                mAct.changeUnit("lbs");
                            }
                        });
                    } else {
                        weightSeparator = secondStripString.indexOf(",kg");
                        mHandler.post(new Runnable() {
                            public void run() {
                                mAct.changeUnit("kg");
                            }
                        });
                    }

                    String rawWeight = secondStripString.substring(0, weightSeparator);
                    String weightString = rawWeight.replaceAll("[^0-9\\.]+", "");

                    BluetoothJsonString = "";
                    Log.d("EID", EIDString);
                    Log.d("VID", VIDString);
                    Log.d("WEIGHT", weightString);

                    if (EIDString!=null && EIDString.length() > 0) {
                        if(EIDString.length() > 15){
                            EIDString = EIDString.substring(0, 15);
                        }
                    } else{
                        EIDString = "000000000000000";
                    }

                    if(VIDString!=null && VIDString.length() > 0){
                        //process as normal
                    } else{
                        VIDString = "000";
                    }

                    //Store even empty eid and vid
                    eid_array.add(EIDString);
                    vid_array.add(VIDString);
                    // parse Weight
                    double weight_value = 0.0;
                    try {
                        weight_value = Double.parseDouble(weightString);
                    } catch (NumberFormatException nfe) {
                        Log.e("RecordsActivity", "Unable to convert weight from string to double");
                    }
                    Log.d("Stable weight", String.valueOf(weight_value));
                    if ((weight_value > 15 && !waitingForScaleToZeroBeforeNextAnimal) || overrideWeightWait) {  // common sense testing so a 0kg value doesn't trigger a dose calibration
                        // don't send another weight until this animal clears the scales and they are reset to 0kg
                        if(bufferMode == true){
                            weight_array.add(weight_value);
                        } else{
                            if(weight_array.size() <=0){
                                weight_array.add(0.0);
                            }
                            weight_array.set(0,weight_value);

                            //Just for XR5000 to override current weight
                            overrideWeightWait = true;
                            waitingForScaleToZeroBeforeNextAnimal = false;
                            //To make sure the adapters configuring to next dose
                            for(ConnectedDevices dev:SelectedDevices){
                                dev.setVarWeightDoseDone(true);
                            }
                        }
                    } else if (weight_value < 15) {
                        waitingForScaleToZeroBeforeNextAnimal = false;
                        Log.d("Discard low weight", String.valueOf(weight_value));
                    } else {
                        Log.d("Ignore weight", String.valueOf(weight_value));
                    }
                }
                else// unstable reading
                {
                  /*  for(ConnectedDevices dev:SelectedDevices) {
                        if (dev.isVarWeightOn()) {
                            if (dev.getVarWeightDoseDone()) {
                                dev.setVarWeightNextDose(true);
                                dev.setVarWeightDoseDone(false);
                            }
                        }
                    }*/
                }
                overrideWeightWait = false;
            }
        });
        Connection.start();
    }

    @Override
    public void run() {
        while(configuring){
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            for(ConnectedDevices dev:SelectedDevices){
                if (dev.getRetreatedFlag() == true) {
                    retreating = true;
                    break;
                } else{
                    retreating = false;
                }
            }
            for(ConnectedDevices dev:SelectedDevices){
                if (dev.getVarWeightDoseDone()|| dev.getDoseType().equalsIgnoreCase("fix")) {
                    //Ready for the next dose
                    doseStatusNextDone = true;
                } else{
                    //Not ready for the next dose
                    doseStatusNextDone = false;
                    break;
                }
            }
            //To skip the first attempt trying to retrieve empty weight and make sure the list not popping out when we already fetch the previous weight in a loop
            if(doseStatusNextDone && weight_array.size()>0 && weight <= -999.0 && NextWeightListOn == true){
                fetchingNewWeight();
            }
            //Displaying data in buffer mode
            if(bufferMode == true){
                if(weight_array.size() <=0 ){
                    previous_weight = 0;
                    previous_vid = "000";
                    previous_eid = "000000000000000";
                } else{
                    if(weight > 0){
                        previous_weight = weight;
                        previous_eid = eid;
                        previous_vid = vid;
                    }
                }
                mAct.setWeightDisplay(String.valueOf(previous_weight+" <- "+weight_array.toString()));
                mAct.setVIDDisplay(previous_vid);
                mAct.setEIDDisplay(previous_eid);
            }
            if(retreating == false) {
                if(weight > 0) {
                    for (ConnectedDevices dev : SelectedDevices) {
                        if(dev.getIndividualRFIDMode()!=3){
                            dev.setanimalEID(eid);
                        }
                        dev.setanimalVID(vid);
                        if (dev != null && dev.getDefaultMed() != null && dev.getDoseType().equalsIgnoreCase("weight")) {
                            if (doseStatusNextDone == true) {

                                //Sharing weights, eid and vid
                                dev.setWeight(weight);

                                //Displaying data in normal mode
                                if (bufferMode == false) {
                                    mAct.setWeightDisplay(String.valueOf(weight));
                                    mAct.setVIDDisplay(vid);
                                    mAct.setEIDDisplay(eid);
                                }
                                if (eid.length() > 0 && !eid.equals("000000000000000")) {
                                    dev.setIndividualRFIDMode(2);
                                }

                                //Set next dose and dose done to false
                                dev.SetVarWeightMode();
                                final LdMedInventory med = dev.getDefaultMed();
                                //Return the number of Kg per Mil
                                double wdose = dev.getWeightDose();

                                Log.d("WDose1",String.valueOf(weight));
                                Log.d("WDose2",String.valueOf(wdose));

                                Double fullDoseToAdminister = 0.0;
                                SharedPreferences shared_unit = mAppContext.getSharedPreferences("unit", mAppContext.MODE_PRIVATE);
                                unit_string = shared_unit.getString("unit_object", null);
                                unit_object = new Gson().fromJson(unit_string, String.class);
                                if (unit_object != null && unit_object.equalsIgnoreCase("lbs")) {
                                    fullDoseToAdminister = new Double((weight * 0.453592) / wdose);
                                    dev.setUnit("lbs");
                                } else {
                                    fullDoseToAdminister = new Double(weight / wdose);
                                    dev.setUnit("kg");
                                }
                                fullDoseToAdminister = round(fullDoseToAdminister, 2);
                                Double remainingDoseToAdminister = new Double(fullDoseToAdminister);
                                med.setAmDose(fullDoseToAdminister);// update the dose
                                dev.setTotalDose(fullDoseToAdminister);
                                mAct.setDoseDisplay(String.valueOf(Math.round(fullDoseToAdminister * 100.0) / 100.0),dev.getId());

                                Double maximumAdapterDose = new Double(remainingDoseToAdminister);
                                Log.d("Check Adap",String.valueOf(dev.getAdapSettings().getMaxDose()));
                                if (dev.getAdapSettings() != null) {
                                    maximumAdapterDose = 1.00 * dev.getAdapSettings().getMaxDose();
                                    if (remainingDoseToAdminister > maximumAdapterDose) {
                                        // issue a full adapter as the main dose
                                        med.setAmDose(maximumAdapterDose);
                                        remainingDoseToAdminister -= maximumAdapterDose;
                                        LdMedInventory remMed = new LdMedInventory(med);
                                        remMed.setAmDose(remainingDoseToAdminister);
                                        dev.setRemainingMed(remMed);
                                    } else {
                                        // issue actual dose as only dose, no remaining dose
                                        med.setAmDose(remainingDoseToAdminister);
                                        dev.setRemainingMed(null);
                                    }
                                    //Turn rfid on again if it is individual mode
                                    if(dev.getIndividualRFIDMode() == 1 && dev.isRfidOn() == false){
                                        dev.TurnRfidOn();
                                        try{
                                            Thread.sleep(500);
                                        } catch(Exception e){

                                        }
                                    }
                                    dev.ConfigureAdapter(med, true, mAppContext);
                                }
                            }
                        }
                    }
                    //Clear out weights until next weight comes in
                    weight = -999.0;
                }
            }
            try {
                Thread.sleep(500);
            }catch(InterruptedException e)
            {
                Log.e("BT Enable","BT Enabled Err" + e);
            }
        }
    }

    public void fetchingNewWeight(){
        if(bufferMode == true){
            if(!NewWeightListOn  && NextWeightListOn){
                mHandler.post(new Runnable(){
                    public void run(){
                        RelativeLayout linearLayout = new RelativeLayout(mAppContext);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                        RelativeLayout.LayoutParams doseValueParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        doseValueParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                        linearLayout.setLayoutParams(params);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mAppContext);
                        alertDialogBuilder.setTitle("Processing new weight: "+ String.valueOf(weight_array.get(0)));
                        String vid_msg = "000";
                        String eid_msg = "000000000000000";
                        if(vid_array.size() > 0){
                            vid_msg = vid_array.get(0);
                        }
                        if(eid_array.size() > 0){
                            eid_msg = eid_array.get(0);
                        }
                        alertDialogBuilder.setMessage("\nVID: "+vid_msg+"\nEID: "+eid_msg);
                        alertDialogBuilder.setView(linearLayout);
                        alertDialogBuilder
                                .setCancelable(false)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                weight = weight_array.remove(0);
                                                eid = "";
                                                vid = "";
                                                if(eid_array.size() > 0){
                                                    eid = eid_array.remove(0);
                                                } else{
                                                    eid = "000000000000000";
                                                }
                                                if(vid_array.size() > 0){
                                                    vid = vid_array.remove(0);
                                                } else{
                                                    vid = "000";
                                                }
                                                dialog.dismiss();
                                                NewWeightListOn = false;
                                                NextWeightListOn = true;
                                            }
                                        })
                                .setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                dialog.cancel();
                                                NewWeightListOn = false;
                                                NextWeightListOn = false;
                                            }
                                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });
                NewWeightListOn = true;
            }
        } else{
            weight = weight_array.remove(0);
            if(eid_array.size() > 0){
                eid = eid_array.remove(0);
            } else{
                eid = "000000000000000";
            }
            if(vid_array.size() > 0){
                vid = vid_array.remove(0);
            } else{
                vid = "000";
            }
        }
    }

    public void setConfiguring(boolean configuring){
        this.configuring = configuring;
    }

    public void stopEnvironment(){
        interrupt();
        Connection.disconnectBluetooth();
        configuring = false;
        SelectedDevices.clear();
    }
    private void showToast(final String message, final Context context) {
        mHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void resetWeight(){
        if(weight_array.size() > 0){
            weight_array.remove(weight_array.size()-1);
        }
        //Perform immediate config change if there is no weight in the array
        if(weight_array.size()<=0){
            overrideWeightWait = true;
            waitingForScaleToZeroBeforeNextAnimal = false;
            //To make sure the adapters configuring to next dose
            for(ConnectedDevices dev:SelectedDevices){
                dev.setVarWeightNextDose(true);
            }
        }
        mAct.setWeightDisplay("0.0");
        mAct.setDoseDisplay("0");
        weight = -999.0;
        NextWeightListOn = true;
    }

    public void resetTreatment(){
        weight = -999.0;
        NextWeightListOn = true;
    }

    public void setListener(OnReadListener listener) {
        this.listener = listener;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
