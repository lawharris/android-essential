package io.automed.com.android_essential.model.environments.json;

/**
 * Created by harris on 6/11/17.
 */
public class StockaID {
    private String animalID;
    private double animalWeigh;
    private double dose;
    private String drugBatch;
    private String drugName;
    private int amID;
    private String treatmentDate;
    private int incomplete;

    public String getAnimalID(){return animalID;}
    public void setAnimalID(String animalID){ this.animalID = animalID;}

    public double getAnimalWeigh(){return animalWeigh;}
    public void setAnimalWeigh(double animalWeigh){ this.animalWeigh = animalWeigh;}

    public double getDose(){return dose;}
    public void setDose(double dose){ this.dose = dose;}

    public String getDrugBatch(){return drugBatch;}
    public void setDrugBatch(String drugBatch){ this.drugBatch = drugBatch;}

    public String getDrugName(){return drugName;}
    public void setDrugName(String drugName){ this.drugName = drugName;}

    public int getAmID(){return amID;}
    public void setAmID(int amID){ this.amID = amID;}

    public String getTreatmentDate(){return treatmentDate;}
    public void setTreatmentDate(String treatmentDate){ this.treatmentDate = treatmentDate;}

    public int getIncomplete(){return incomplete;}
    public void setIncomplete(int incomplete){ this.incomplete = incomplete;}
}
