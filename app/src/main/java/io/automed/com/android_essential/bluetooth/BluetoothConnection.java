package io.automed.com.android_essential.bluetooth;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.util.Set;
import java.util.UUID;


public class BluetoothConnection extends Thread {
    public interface OnReadListener {
        void onRead(byte[] response, int len);
    }

    String DeviceName;
    OnReadListener listener = null;
    BluetoothDevice btDevice = null;
    BluetoothSocket btSocket;
    DataOutputStream out;
    InputStream in;
    boolean Running;
    boolean SocketError;
    private Context mAppContext;
    private Boolean requireConfirmSending = true;
    private Boolean deviceConnected = false;
    private String confirmSendingSignal="";
    private Boolean weightScale = false;


    public BluetoothConnection(String DeviceName, Context AppContext, Boolean requireConfirmSending, String confirmSendingSignal, Boolean weightScale) {
        this.DeviceName = DeviceName;
        btDevice = null;
        listener = null;
        this.Running = false;
        SocketError = false;
        mAppContext = AppContext;
        this.requireConfirmSending = requireConfirmSending;
        this.confirmSendingSignal = confirmSendingSignal;
        this.weightScale = weightScale;
    }

    public void SendCmd(String cmd){
        int connTimeOut=0;
        while(!Running) {
            try {
                connTimeOut++;
                Thread.sleep(100);
                if(connTimeOut==5)
                    break;
            }catch(InterruptedException e){
                    Log.e("Bluetooth Send Cmd", "Failed" + e);
            }
        }
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(btSocket.getOutputStream(), "ASCII"));
            writer.write(cmd);
            writer.flush();
        } catch (IOException ex) {
            Log.e("SPP", "Failed to write a command: " + ex);
            SocketError = true;
            return;
        }
    }
/*
    public Runnable PeriodicRead = new Runnable() {
        @Override
        public void run() {
            while(Running)
            {
                SendCmd("{RP}");
                try {
                    Thread.sleep(200);
                }catch(InterruptedException e)
                {
                }
            }
        }
    };
*/
 /*
 public void SPP()
    {
        final String command = "{RP}";


        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(btSocket.getOutputStream(), "ASCII"));
            writer.write(command);
            writer.flush();
        } catch (IOException ex) {
            Log.d("SPP", "Failed to write a command: " + ex.toString());
            return;
        }
        Log.d("SPP", "Command is sent: " + command);

        char output;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(btSocket.getInputStream(), "ASCII"));
            output = (char)reader.read();
        } catch (IOException ex) {
            Log.d("SPP", "Failed to write a command: " + ex.toString());
            return;
        }
        Log.d("SPP", "Result: " + output);

        try {
            btSocket.close();
        } catch (IOException ex) {
            Log.d("SPP", "Failed to close the bluetooth socket.");
            return;
        }
        Log.d("SPP", "Closed the bluetooth socket.");
    }
 */

    /*private void showToast(String text){
        Message msg =  new Message();
        Bundle bundle = new Bundle();

        bundle.putInt(AppConstants.HANDLER_MSG_TYPE, AppConstants.MSGTypes.MSG_TYPE_TOAST.ordinal());
        bundle.putString(AppConstants.HANDLER_TOAST_MSG, text);
        msg.setData(bundle);
        mHandler.sendMessage(msg);

    }*/

    public void disconnectBluetooth() {
        // if bluetooth is connected, try to disconnect it
        if (btSocket != null) {
            try {
                deviceConnected = false;
                btSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {

        final UUID sppUuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        deviceConnected = false;

        try {
            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

            if (btAdapter == null) {
                Log.d("SPP", "Bluetooth adapter is not available.");
                showToast("Bluetooth adapter is not available.",mAppContext);
                return;
            }
            //Log.d("SPP", "Bluetooth adapter is found.");
            // JH 161028 - why are we disabling the adapter if it's on?
            /*
            if (btAdapter.isEnabled()) {
                btAdapter.disable();
                try {
                    Thread.sleep(1000);
                }catch(InterruptedException e)
                {
                    Log.e("BT Disable","BT Disabled Err" + e);
                }
            }
            */
            if (!btAdapter.isEnabled()) {
                showToast("Enabling Bluetooth",mAppContext);
                btAdapter.enable();
                try {
                    Thread.sleep(1000);
                }catch(InterruptedException e)
                {
                    Log.e("BT Enable","BT Enabled Err" + e);
                }
            }

            if (!btAdapter.isEnabled()) {
                //Log.d("SPP", "Bluetooth is disabled. Check configuration.");
                return;
            }else {
               // Log.d("SPP", "Bluetooth is enabled.");
                showToast("Bluetooth is enabled.",mAppContext);
            }

            Set<BluetoothDevice> bondedDevices = btAdapter.getBondedDevices();
            for (BluetoothDevice dev : bondedDevices) {
                //Log.d("SPP", "Paired device: " + dev.getName() + " (" + dev.getAddress() + ")");
                if (dev.getName().equals(DeviceName)) {
                    btDevice = dev;
                    break;
                }
            }

            if (btDevice == null) {
                //Log.d("SPP", "Target Bluetooth device is not found.");
                showToast("Target Bluetooth device is not found.",mAppContext);
                return;
            }
            //Log.d("SPP", "Target Bluetooth device is found.");
            showToast("Bluetooth Device Found",mAppContext);

            try {
                btSocket = btDevice.createRfcommSocketToServiceRecord(sppUuid);
            } catch (IOException ex) {
                showToast("Failed to create Bluetooth socket: ",mAppContext);
                        Log.e("SPP", "Failed to create RfComm socket: " ,ex);
                return;
            }
            //Log.d("SPP", "Created a bluetooth socket.");

            for (int i = 0; ; i++) {
                try {
                    btSocket.connect();
                } catch (IOException ex) {
                    if (i < 5) {
                        Log.e("SPP", "Failed to connect. Retrying: " ,ex);
                        showToast("Bluetooth Failed to connect. Retrying: ",mAppContext);
                        continue;
                    }
                    Log.e("SPP", "Failed to connect. Retrying: ", ex);
                    return;
                }
                break;
            }

            showToast("Connected to "+ btDevice.getName(),mAppContext);
                Log.d("SPP", "Connected to the bluetooth socket.");


            out = new DataOutputStream(btSocket.getOutputStream());
            in = btSocket.getInputStream();

            byte[] Buffer = new byte[128];
            int len=0;
            Running = true;
            DataInputStream dis = new DataInputStream(in);
            // setup code for Jindalee
            // scales constantly push data, no need to do anything but listen
            SendCmd("{ZA1}");
            SendCmd("{ZE1}");
            SendCmd("{VA}");
            SendCmd("{VC}");
            SendCmd("{SPRO2}");
            SendCmd("{SPRP2}");
            if(requireConfirmSending == true){
                SendCmd(confirmSendingSignal);
            }
            while (btSocket.isConnected() && !Thread.currentThread().isInterrupted() && !SocketError)
            {
                if(deviceConnected == false && weightScale == true){
                    Handler h = new Handler(mAppContext.getMainLooper());
                    h.post(new Runnable() {
                        public void run() {
                            AlertDialog alertDialog = new AlertDialog.Builder(mAppContext).create();
                            alertDialog.setTitle("Alert");
                            alertDialog.setMessage("Connection Successful. Waiting for Animal Weight");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
                    });
                    deviceConnected = true;
                }

                //read response with timeout
               // SendCmd("{RW}");
                //SendCmd("{SPRP}");
                //

                // just listen at Jindalee

                //SendCmd("{SPRP2}");
                //SendCmd("{SPRO2}");
                //SendCmd("{RP}");
                //SendCmd("{RW}");

                try {
                    Thread.sleep(400);
                }catch(InterruptedException e)
                {
                    Log.e("Send Cmd {RW}","Try Thread Sleep" + e);
                }
                if(in.available()>0)
                {
                    try {
                        len=dis.read(Buffer);
                        if(len== -1) {
                            Log.d("Bluetooth Task", " Closed");
                            Running=false;
                            out.flush();
                            out.close();
                            in.close();
                            btSocket.close();
                            break;
                        }else{
                            if(len>0){
                                if (listener != null) {
                                    Log.d("BT Pkt Len", String.valueOf(len));
                                    listener.onRead(Buffer,len);
                                }
                            }
                        }
                    }catch(SocketTimeoutException e){
                        Log.d("TCP Task", " Time Out");
                    }catch (Exception e){
                        Log.e("Bluetooth Task", " Error", e);
                    }
                    finally {
                        //Log.d("Successfully Completed", "RUN");
                    }
                }
//                String output;
//                try {
//                    BufferedReader reader = new BufferedReader(new InputStreamReader(btSocket.getInputStream(), "ASCII"));
//                    output = reader.readLine();
//                } catch (IOException ex) {
//                    Log.d("SPP", "Failed to write a command: " + ex.toString());
//                    return;
//                }
//                Log.d("SPP", "Result: " + output);
            }
            if(btSocket!=null){
                Log.d("BlueTooth Task", "Stopping");
                Running=false;
                out.flush();
                out.close();
                in.close();
                btSocket.close();
            }
        } catch (Exception e) {
            Log.e("TCPConnection","error in Bluetooth",e);
        }
    }

    private void showToast(final String message, final Context context) {
        Handler h = new Handler(mAppContext.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setListener(OnReadListener listener) {
        this.listener = listener;
    }
}
