package io.automed.com.android_essential.wifi;

/**
 * this class represents the informations about the device connected ot the wifi hotspot
 *
 */

public class ClientScanResult {
    private String IP;
    private String MAC;
    private String Interface;

    public String getIP() {
        return IP;
    }
    public String getMAC() {
        return MAC;
    }
    public String getInterface() {
        return Interface;
    }

    public ClientScanResult(String ip, String MAC, String Interface){
        this.IP = ip;
        this.MAC = MAC;
        this.Interface = Interface;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;   //If objects equal, is OK
        if (o instanceof ClientScanResult) {
            ClientScanResult that = (ClientScanResult)o;
            return (IP.equals(that.getIP())  && MAC.equals(that.getMAC()) && Interface.equals(that.getInterface()));
        }
        return false;
    }
}
